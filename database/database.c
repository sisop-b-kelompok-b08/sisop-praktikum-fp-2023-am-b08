#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <dirent.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/types.h>

// PORT socket di database harus sama dengan PORT socket di client
#define PORT 8085
#define LENGTH_OF_USERNAME 512
#define LENGTH_OF_PW 512
#define DirectoryToDatabase "databases"
#define DirectoryToLibrary "databases/lib"
char current_db[512] = "";

int Authenticate(char *username, char *password) {
    char user_list_dir[100];
    sprintf(user_list_dir, "%s/user_list.csv", DirectoryToLibrary);

    FILE *user_list = fopen(user_list_dir, "r");
    if (user_list != NULL) {
        char buf[256];
        char curr_usrname[LENGTH_OF_USERNAME];
        char curr_pw[LENGTH_OF_PW];

        while (fgets(buf, sizeof(buf), user_list)) {
            sscanf(buf, "%[^,],%s", curr_usrname, curr_pw); // ambil kolom pertama dan kolom kedua dari tiap barisnya.
            
            if (strcmp(curr_usrname, username) == 0 && strcmp(curr_pw, password) == 0) { // cek jika sama dengan input
                fclose(user_list);
                return 1; // Berhasil Masuk
            }
        }

        fclose(user_list);
    }
    return 0; // Gagal Masuk
}

int User_Presence(char *username) {
    char user_list_dir[100];
    sprintf(user_list_dir, "%s/user_list.csv", DirectoryToLibrary);

    FILE *user_list = fopen(user_list_dir, "r");
    if (user_list != NULL) {
        char buf[256];
        char curr_usrname[LENGTH_OF_USERNAME];

        while (fgets(buf, sizeof(buf), user_list)) {
            sscanf(buf, "%[^,]", curr_usrname); // Pada file csv, cari kolom pertama, yaitu sebelum koma ditemukan.
            
            if (strcmp(curr_usrname, username) == 0) {
                fclose(user_list);
                return 1; // Username ditemukan
            }
        }
        fclose(user_list);
    }
    return 0; // Username tidak ditemukan
}

void InsertUser(char *username, char *password) {
    char user_list_dir[100];
    sprintf(user_list_dir, "%s/user_list.csv", DirectoryToLibrary);

    FILE *user_list = fopen(user_list_dir, "a");
    if (user_list != NULL) {
        fprintf(user_list, "%s,%s\n", username, password);
        fclose(user_list);
        return;
    } else {
        perror("Gagal mendaftarkan user");
    }
}

int Create_Database(char *dbname) {
    char dbdir[100];
    sprintf(dbdir, "%s/%s", DirectoryToDatabase, dbname);

    int created = mkdir(dbdir, 0777);
    if (created == 0) {
        printf("Database %s berhasil dibuat\n", dbname);
        return 1; 
    } else {
        perror("Gagal membuat Database");
        return 0; 
    }
}

int Grant_Permission(char *username, char *dbname) {
    char dbdir[100];
    sprintf(dbdir, "%s/%s", DirectoryToDatabase, dbname);

    DIR* dir = opendir(dbdir);
    if (dir) {
        closedir(dir);
    } else {
        printf("Database tidak tersedia\n");
        return 1;
    }

    char user_access_dir[100];
    sprintf(user_access_dir, "%s/user_access.csv", DirectoryToLibrary);

    FILE *user_access = fopen(user_access_dir, "a");
    if (user_access != NULL) {
        fprintf(user_access, "%s,%s\n", username, dbname);
        fclose(user_access);
        printf("Berhasil memberikan permission user %s ke database %s\n", username, dbname);
        return 0;
    } else {
        printf("Gagal memberikan permission");
        return 1;        
    }
}

int Check_Permission(char *username, char *dbname) {
    char user_access_dir[100];
    sprintf(user_access_dir, "%s/user_access.csv", DirectoryToLibrary);

    FILE *user_access = fopen(user_access_dir, "r");
    if (user_access != NULL) {
        char buf[512];
        char curr_usrname[LENGTH_OF_USERNAME];
        char curr_db[512];

        while (fgets(buf, sizeof(buf), user_access)) {
            sscanf(buf, "%[^,],%s", curr_usrname, curr_db); // ambil setiap username dan db_name dari setiap line

            if (strcmp(curr_usrname, username) == 0 && strcmp(curr_db, dbname) == 0) {
                fclose(user_access);
                return 1; 
            }
        }

        fclose(user_access);
    }

    return 0; // User does not have access rights to the database
}

int Drop_DB(char *dbname){
    char dbdir[512];
    sprintf(dbdir, "%s/%s", DirectoryToDatabase, dbname);
    
    char cmd[1024];
    sprintf(cmd, "rm -rf %s", dbdir);
    int status = system(cmd);
    if (status == 0) {
        printf("Berhasil untuk remove database %s\n", dbname);
    } else {
        perror("Gagal untuk remove database");
        
    }
}

void CreateDB_Folder(){
    // buat folder database (menyimpan database yang dibuat dalam bentuk .csv)
    int dbdir = mkdir(DirectoryToDatabase, 0777); // read write execute for everyone
    
    // buat folder akses database (menyimpan user_list.csv dan user_access.csv)
    int accessdir = mkdir(DirectoryToLibrary, 0700);
    
    // buat csv yang menyimpan kredensial user dan password
    char user_list_dir[100];
    sprintf(user_list_dir, "%s/user_list.csv", DirectoryToLibrary);

    FILE *user_list = fopen(user_list_dir, "a");
    if (user_list == NULL) {
        perror("Gagal membuat file user_list.csv");
        exit(EXIT_FAILURE);
    }
    fclose(user_list);

    // buat csv yang menyimpan akses dari user-user terdaftar
    char user_access_dir[100];
    sprintf(user_access_dir, "%s/user_access.csv", DirectoryToLibrary);

    FILE *user_access = fopen(user_access_dir, "a");
    if (user_access == NULL) {
        perror("Gagal membuat file user_access.csv");
        exit(EXIT_FAILURE);
    }
    fclose(user_access);
}

int connect_socket(){
    // Connect ke socket agar terhubung nantinya dengan client
    int socker_serv, accept_socket;
    struct sockaddr_in socket_addr;

    int addrlen = sizeof(socket_addr);
    
    if ((socker_serv = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Gagal membuat socket server");
        exit(EXIT_FAILURE);
    }

    socket_addr.sin_port = htons(PORT);
    socket_addr.sin_family = AF_INET;
    socket_addr.sin_addr.s_addr = INADDR_ANY;
    
    if (bind(socker_serv, (struct sockaddr *)&socket_addr, sizeof(socket_addr)) < 0) {
        perror("Gagal melakukan bind ke socket");
        exit(EXIT_FAILURE);
    }

    if (listen(socker_serv, 3) < 0) {
        perror("Gagal melakukan listen ke socket");
        exit(EXIT_FAILURE);
    }

    printf("Server Database Siap Digunakan\n");

    if ((accept_socket = accept(socker_serv, (struct sockaddr *)&socket_addr, (socklen_t*)&addrlen)) < 0) {
        perror("Gagal melakukan accept ke socket");
        exit(EXIT_FAILURE);
    }

    return accept_socket;
}

int main() {
    CreateDB_Folder(); // buat folder keperluan database
    int accept_socket = connect_socket(); // connect ke socket agar terhubung dengan client nantinya
    
    char username[512], password[512];
    char retrieveCommand[512];

    while (1){
        memset(retrieveCommand, 0, sizeof(retrieveCommand));
        int read_socket = read(accept_socket, retrieveCommand, sizeof(retrieveCommand) - 1); // ambil argumen yang ada di socket, simpan ke retrieveCommand

        char command[1024], rest_arg[1024];
        strcpy(command, retrieveCommand); // string menyimpan tipe command nya apa (authenticate, create user, dsbg.)
        strcpy(rest_arg, retrieveCommand); // string yang menyimpan sisa argumen diluar command (username, password, dsbg.)

        if (strstr(command, "authenticate") != NULL){ // command autentikasi (masuk sebagai user apa)
            char *tokenize = strtok(rest_arg, " "); // penggal kata pertama (authenticate)
            tokenize = strtok(NULL, " "); // ambil kata kedua (username)
            strcpy(username, tokenize);
            tokenize = strtok(NULL, " "); // ambil kata ketiga (password)
            strcpy(password, tokenize);

            if (password[strlen(password)-1] == '\n') password[strlen(password)-1] = '\0';

            int userRoot = strcmp(username, "root");
            int verified = Authenticate(username, password);

            if((userRoot == 0) || (verified == 1)){
                printf("Autentikasi Berhasil, Selamat Datang di Database %s!\n", username);
            }else{
                printf("Autentikasi Gagal, coba lagi!\n"); 
            }
        }

        else if (strstr(command, "CREATE USER") != NULL){ // command buat user baru
            int userRoot = strcmp(username, "root");

            if(userRoot == 0) { // hanya root yang bisa melakukan create user
                char new_user[512], new_pass[512];

                // Penggal kata CREATE dan USER
                char *tokenize = strtok(rest_arg, " ");
                tokenize = strtok(NULL, " ");

                // Ambil username
                tokenize = strtok(NULL, " ");
                strcpy(new_user, tokenize);
                
                // Penggal kata IDENTIFIED BY
                tokenize = strtok(NULL, " ");
                tokenize = strtok(NULL, " ");
                // Ambil password
                tokenize = strtok(NULL, ";");
                strcpy(new_pass, tokenize);
                
                if (User_Presence(new_user)) {
                    printf("Username sudah pernah digunakan, coba lagi!\n");
                } else {
                    InsertUser(new_user, new_pass);
                    printf("Berhasil mendaftarkan user %s ke database.\n", new_user);
                }

            } else {
                printf("Gagal melakukan CREATE USER, coba lagi!\n");
            }

        } else if(strstr(command, "CREATE DATABASE") != NULL){ // command membuat database
            // Penggal kata CREATE dan DATABASE
			char *tokenize = strtok(rest_arg, " ");
        	tokenize = strtok(NULL, " ");

            // Ambil Nama Database
            char new_db[512];
        	tokenize = strtok(NULL, " ");
        	tokenize = strtok(tokenize, ";"); // remove semicolon
            strcpy(new_db, tokenize);

        	Create_Database(new_db);

            int userRoot = strcmp(username, "root");

            if(userRoot != 0){  
                Grant_Permission(username, new_db); // beri akses ke pembuat database
            } 

            Grant_Permission("root", new_db); // root otomatis memiliki akses terhadap database tersebut

		} else if(strstr(command, "USE") != NULL) { // command USE untuk mengakses database 
            // Penggal kata USE
        	char *tokenize = strtok(rest_arg, " "); 

            // Ambil [nama_db];
        	tokenize = strtok(NULL, " ");
            // Remove semicolon                                                                       
        	char *dbname = strtok(tokenize, ";");

            int userRoot = strcmp(username, "root");
            int have_permission = Check_Permission(username, dbname);

        	if (userRoot == 0 || have_permission == 1){ // jika root ATAU memiliki permission
        		strcpy(current_db, dbname); // ganti context database saat ini 
                printf("User %s berhasil akses ke database %s", username, dbname);
			} else {
				printf("Gagal mengakses database %s\n", dbname);
			}

		} else if(strstr(command, "GRANT PERMISSION")){ // command untuk memberi permission ke suatu user
            // Penggal kata GRANT dan PERMISSION
            char *tokenize = strtok(rest_arg, " ");
        	tokenize = strtok(NULL, " ");

            // Ambil nama user
            char new_user[512], dbname[512];
            tokenize = strtok(NULL, " ");
            strcpy(dbname, tokenize);
            // Penggal kata INTO
            tokenize = strtok(NULL, " ");
            // Ambil nama DB, hapus semicolon
            tokenize = strtok(NULL, ";");
            strcpy(new_user, tokenize);

            int userRoot = strcmp(username, "root");

            if ((userRoot == 0) && User_Presence(new_user)) {
                Grant_Permission(new_user, dbname);
            } else {
                printf("Gagal memberi permission\n");
            }
        } else if(strstr(command, "DROP") != NULL) { // command drop untuk hapus
            if(strstr(command, "DATABASE") != NULL){ // hapus database
                char dbname[512];

                // Penggal kata DROP dan DATABASE
                char *tokenize = strtok(rest_arg, " ");
                tokenize = strtok(NULL, " ");

                // Ambil nama database dan remove semicolon
                tokenize = strtok(NULL, ";");
                strcpy(dbname, tokenize);

                int have_permission = Check_Permission(username, dbname);

                if (have_permission == 1) {
                    Drop_DB(dbname);
                } else {
                    printf("User %s tidak memiliki akses ke database %s\n", username, dbname);
                }
            }
            
        } else printf("Command tidak valid, coba lagi!");
    }
    
    return 0;
}