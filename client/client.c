#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

int connect_socket(){
    int HOST_PORT = 8085;
    char IPADRESS[20] = "127.0.0.1";

    struct sockaddr_in sock_address;
    char buffer[1024];
    int sock = 0;

    // Buat Socket
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Pembuatan Socket Gagal\n");
        exit(EXIT_FAILURE);
    }

    sock_address.sin_family = AF_INET;
    sock_address.sin_port = htons(HOST_PORT);

    // Ubah alamat IPv4 dan IPv6 dari text ke binary form
    if (inet_pton(AF_INET, IPADRESS, &sock_address.sin_addr) <= 0) {
        printf("IP Adress Tidak Valid\n");
        exit(EXIT_FAILURE);
    }

    // Koneksi ke Server
    if (connect(sock, (struct sockaddr *)&sock_address, sizeof(sock_address)) < 0) {
        printf("Koneksi Sock Gagal, PORT Invalid\n");
        exit(EXIT_FAILURE);
    }

    return sock;

}

int main(int argc, char *argv[]) {
    if (argc != 5) {
        printf("Command tidak valid\n");
        printf("Gunakan: %s -u [username] -p [password]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    char *username = argv[2]; // ./client -u [2] -p [4]
    char *password = argv[4]; // usernama ada di index 2, password di index 4

    // Socket Connection, client akan ke socket yang sama dengan database.
    int sock = connect_socket();
    
    char authenticate[150];
    if(username == "root"){ // cek apakah user adalah root 
        sprintf(authenticate, "authenticate root superuser");
    } else {
        sprintf(authenticate, "authenticate %s %s", username, password);
    }
    
    // kirim pesan ke socket untuk diterima database.c
    printf("Authenticate Command: %s\n", authenticate);
    send(sock, authenticate, strlen(authenticate), 0);

    char command[1024];
    while(1){
        fgets(command, sizeof(command), stdin);
        printf("Command from client: %s\n", command);
        send(sock, command, strlen(command), 0);
    }

    close(sock);

    return 0;
}
