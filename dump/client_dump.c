#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>

#define PORT 8085

bool isSocketClosed(int readValue, int socketDescriptor);
bool validateArguments(int argc, char const *argv[]);
int createSocketAndConnect(struct sockaddr_in *serverAddress);
void sendData(int socketDescriptor, const char *data);
bool receiveData(int socketDescriptor, char *buffer, int bufferSize);
void handleDatabaseChange(int socketDescriptor, const char *databaseName);

int main(int argc, char const *argv[]) {
    if (!validateArguments(argc, argv)) {
        printf("Error: Invalid arguments.\n");
        return -1;
    }

    struct sockaddr_in serverAddress;
    int socketDescriptor = createSocketAndConnect(&serverAddress);
    if (socketDescriptor < 0) {
        printf("Error: Connection failed.\n");
        return -1;
    }

    char type[1024];
    if (getuid()) {
        sprintf(type, "%s %s dump", argv[2], argv[4]);
    } else {
        strcpy(type, "root dump");
    }
    sendData(socketDescriptor, type);

    char statusLogin[1000];
    if (!receiveData(socketDescriptor, statusLogin, sizeof(statusLogin))) {
        close(socketDescriptor);
        printf("Error: Authentication failed.\n");
        return -1;
    }

    char command[1000];
    sprintf(command, "USE %s", getuid() ? argv[5] : argv[1]);
    sendData(socketDescriptor, command);

    char receive[1000];
    strcpy(command, "continue");
    sendData(socketDescriptor, command);

    if (strncmp(receive, "Database changed to", 19) == 0) {
        handleDatabaseChange(socketDescriptor, argv[5]);
        close(socketDescriptor);
        return 0;
    }

    close(socketDescriptor);
    return 0;
}

bool validateArguments(int argc, char const *argv[]) {
    if (getuid()) {
        if (argc < 6 || (strcmp(argv[1], "-u") != 0) || (strcmp(argv[3], "-p") != 0)) {
            return false;
        }
    } else {
        if (argc < 2) {
            return false;
        }
    }
    return true;
}

int createSocketAndConnect(struct sockaddr_in *serverAddress) {
    int socketDescriptor;
    if ((socketDescriptor = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        return -1;
    }

    memset(serverAddress, '0', sizeof(*serverAddress));
    serverAddress->sin_family = AF_INET;
    serverAddress->sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &(serverAddress->sin_addr)) <= 0) {
        return -1;
    }

    if (connect(socketDescriptor, (struct sockaddr *)serverAddress, sizeof(*serverAddress)) < 0) {
        return -1;
    }

    return socketDescriptor;
}

void sendData(int socketDescriptor, const char *data) {
    send(socketDescriptor, data, strlen(data), 0);
}

bool receiveData(int socketDescriptor, char *buffer, int bufferSize) {
    int readValue = recv(socketDescriptor, buffer, bufferSize, 0);
    if (readValue == 0) {
        close(socketDescriptor);
        return false;
    }
    return true;
}

void handleDatabaseChange(int socketDescriptor, const char *databaseName) {
    char receive[1000];
    do {
        memset(receive, 0, sizeof(receive));
        receiveData(socketDescriptor, receive, sizeof(receive));
        if (strcmp(receive, "Done") != 0) {
            printf("%s\n", receive);
            char command[1000];
            strcpy(command, "continue");
            sendData(socketDescriptor, command);
        }
    } while (strcmp(receive, "Done") != 0);
}

bool isSocketClosed(int readValue, int socketDescriptor) {
    if (readValue == 0) {
        close(socketDescriptor);
        return true;
    }
    return false;
}
