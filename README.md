# Praktikum Modul 3 Sistem Operasi

Perkenalkan kami dari kelas ``Sistem Operasi B Kelompok  B08``, dengan Anggota sebagai berikut:

| Nama                      | NRP        |
|---------------------------|------------|
|Dimas Prihady Setyawan     | 5025211184 |
|Yusuf Hasan Nazila         | 5025211225 |
|Ahda Filza Ghaffaru        | 5025211144 |

# Penjelasan FP
# Penjelasan ``client``
Kode dari ``client.c`` adalah program klien sederhana yang menggunakan soket untuk terhubung ke server. Berikut adalah ringkasan dari kode tersebut:

1. Beberapa header file termasuk ``stdio.h, stdlib.h, string.h, stdbool.h, unistd.h, sys/socket.h, dan arpa/inet.h`` diimpor.

2. Fungsi ``connect_socket()`` digunakan untuk membuat koneksi soket dengan server. Fungsi ini mengembalikan deskriptor soket yang terhubung.

3. Fungsi ``main()`` adalah titik masuk program. Pertama, itu memeriksa jumlah argumen baris perintah yang diberikan. Jika tidak ada 5 argumen, program mencetak pesan kesalahan dan keluar.

4. Username dan password diperoleh dari argumen baris perintah.

5. Fungsi ``connect_socket()`` dipanggil untuk membuat koneksi soket ke server. Deskriptor soket yang terhubung disimpan dalam variabel sock.

6. String ``authenticate`` dibentuk berdasarkan username dan password yang diberikan. Jika username adalah "root", string ``authenticate`` akan berisi "authenticate root superuser". Jika bukan, string ``authenticate`` akan berisi "authenticate [username] [password]".

7. String ``authenticate`` dikirim melalui soket ke server menggunakan fungsi ``send()``. Pesan ini akan digunakan untuk otentikasi dengan server.

8. Setelah otentikasi berhasil, program memasuki loop while. Di dalam loop ini, program membaca perintah dari pengguna melalui stdin menggunakan ``fgets()`` dan mengirim perintah tersebut melalui soket ke server menggunakan ``send()``.

9. Loop terus berlanjut sampai program dihentikan atau koneksi dengan server terputus.

10. Setelah loop selesai, koneksi soket ditutup menggunakan ``close()`` dan program keluar dengan kode status 0.

Program ini bertindak sebagai klien yang terhubung ke server menggunakan soket, mengotentikasi dengan server, dan mengirim perintah ke server sesuai input pengguna.

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
```
Berikut adalah penjelasan untuk setiap header yang disertakan dalam kode:

1. ``stdio.h``: Header ini berisi fungsi-fungsi standar untuk input dan output, seperti ``printf()`` dan ``scanf()``.

2. ``stdlib.h``: Header ini berisi fungsi-fungsi standar untuk alokasi memori dinamis, pengelolaan proses, dan konversi angka, seperti ``malloc(), exit(), dan atoi()``.

3. ``string.h``: Header ini berisi fungsi-fungsi untuk manipulasi string, seperti ``strlen(), strcpy(), dan strcmp()``.

4. ``stdbool.h``: Header ini berisi definisi tipe data boolean dan nilai-nilainya (``true dan false``) yang diperkenalkan dalam standar C99.

5. ``unistd.h``: Header ini berisi fungsi-fungsi untuk interaksi dengan sistem operasi, seperti ``close()`` untuk menutup soket atau file, dan sleep() untuk menjeda eksekusi program.

6. ``sys/socket.h``: Header ini berisi definisi dan fungsi-fungsi yang digunakan untuk pemrograman soket (socket programming), seperti membuat soket (``socket()``), mengirim data melalui soket (``send()``), dan menerima data dari soket (``recv()``).

7. ``arpa/inet.h``: Header ini berisi definisi dan fungsi-fungsi untuk operasi jaringan, seperti konversi alamat IP dalam format teks ke format biner (``inet_pton()``) atau sebaliknya (``inet_ntop()``).

Dengan menyertakan header-header ini, program dapat menggunakan fungsi-fungsi dan definisi yang diperlukan untuk berkomunikasi melalui soket, melakukan operasi input/output, manipulasi string, alokasi memori, dan interaksi dengan sistem operasi.

```c
int connect_socket(){
    int HOST_PORT = 8085;
    char IPADRESS[20] = "127.0.0.1";

    struct sockaddr_in sock_address;
    char buffer[1024];
    int sock = 0;

    // Buat Socket
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Pembuatan Socket Gagal\n");
        exit(EXIT_FAILURE);
    }

    sock_address.sin_family = AF_INET;
    sock_address.sin_port = htons(HOST_PORT);

    // Ubah alamat IPv4 dan IPv6 dari text ke binary form
    if (inet_pton(AF_INET, IPADRESS, &sock_address.sin_addr) <= 0) {
        printf("IP Adress Tidak Valid\n");
        exit(EXIT_FAILURE);
    }

    // Koneksi ke Server
    if (connect(sock, (struct sockaddr *)&sock_address, sizeof(sock_address)) < 0) {
        printf("Koneksi Sock Gagal, PORT Invalid\n");
        exit(EXIT_FAILURE);
    }

    return sock;

}
```
Fungsi connect_socket() memiliki tujuan untuk membuat koneksi soket dengan server.

Berikut adalah penjelasan langkah-langkah yang dilakukan oleh fungsi ini:

1. Pertama, fungsi ini mendefinisikan konstanta ``HOST_PORT`` dengan nilai 8085 dan string ``IPADRESS`` dengan nilai ``"127.0.0.1"``. HOST_PORT merupakan port yang akan digunakan untuk koneksi soket, sedangkan ``IPADRESS`` adalah alamat IP server.

2. Selanjutnya, fungsi ini mendeklarasikan variabel-variabel yang diperlukan, yaitu ``sock_address`` yang merupakan struktur ``sockaddr_in`` untuk menyimpan informasi tentang alamat soket, buffer yang berfungsi sebagai ``buffer`` untuk data yang diterima dari server, dan sock yang merupakan descriptor soket.

3. Kemudian, fungsi ini membuat soket menggunakan fungsi ``socket()`` dengan parameter ``AF_INET`` untuk protokol IPv4, ``SOCK_STREAM`` untuk jenis soket TCP, dan nilai 0 untuk protokol default. Jika pembuatan soket gagal, maka program akan mencetak pesan "Pembuatan Socket Gagal" dan keluar dengan menggunakan ``exit(EXIT_FAILURE)``.

4. Selanjutnya, fungsi ini mengatur atribut-atribut soket dengan mengisi nilai pada struktur ``sock_address``. ``sin_family`` diatur ke ``AF_INET`` untuk menunjukkan penggunaan protokol IPv4. ``sin_port`` diatur menggunakan fungsi ``htons()`` untuk mengonversi byte order dari host ke network byte order. Dalam hal ini, ``HOST_PORT`` digunakan sebagai nilai port.

5. Setelah itu, alamat IP dalam format teks (string) diubah menjadi bentuk biner menggunakan fungsi ``inet_pton()``. Jika konversi gagal, maka program mencetak pesan "IP Address Tidak Valid" dan keluar dengan menggunakan ``exit(EXIT_FAILURE)``.

6. Terakhir, fungsi ini melakukan koneksi ke server dengan menggunakan fungsi ``connect()`` dengan parameter sock untuk descriptor soket, ``(struct sockaddr *)&sock_address`` untuk alamat server yang dikonversi menjadi tipe ``struct sockaddr *``, dan ``sizeof(sock_address)`` untuk ukuran alamat. Jika koneksi gagal, maka program mencetak pesan "Koneksi Socket Gagal, PORT Invalid" dan keluar dengan menggunakan ``exit(EXIT_FAILURE)``.

7. Jika semua langkah di atas berhasil, maka fungsi ini akan mengembalikan nilai ``sock``, yaitu descriptor soket yang telah terhubung dengan server.

```c
int main(int argc, char *argv[]) {
    if (argc != 5) {
        printf("Command tidak valid\n");
        printf("Gunakan: %s -u [username] -p [password]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    char *username = argv[2]; // ./client -u [2] -p [4]
    char *password = argv[4]; // usernama ada di index 2, password di index 4

    // Socket Connection, client akan ke socket yang sama dengan database.
    int sock = connect_socket();
    
    char authenticate[150];
    if(username == "root"){ // cek apakah user adalah root 
        sprintf(authenticate, "authenticate root superuser");
    } else {
        sprintf(authenticate, "authenticate %s %s", username, password);
    }
    
    // kirim pesan ke socket untuk diterima database.c
    printf("Authenticate Command: %s\n", authenticate);
    send(sock, authenticate, strlen(authenticate), 0);

    char command[1024];
    while(1){
        fgets(command, sizeof(command), stdin);
        printf("Command from client: %s\n", command);
        send(sock, command, strlen(command), 0);
    }

    close(sock);

    return 0;
}
```
Fungsi main() merupakan fungsi utama yang akan dieksekusi saat program dijalankan. Fungsi ini berperan dalam mengatur alur program dan menjalankan operasi utama.

Berikut adalah penjelasan langkah-langkah yang dilakukan oleh fungsi ini:

1. Pertama, fungsi ini menerima argumen baris perintah melalui parameter ``argc`` (jumlah argumen) dan ``argv`` (array argumen).

2. Fungsi ini melakukan pemeriksaan kondisi dengan memeriksa apakah ``argc`` tidak sama dengan 5, yang berarti argumen yang diberikan tidak valid. Jika tidak valid, program akan mencetak pesan "Command tidak valid" dan "Gunakan: %s -u [username] -p [password]" dengan menggunakan ``printf()``. Setelah itu, program keluar dengan menggunakan ``exit(EXIT_FAILURE)``.

3. Jika argumen valid, fungsi ini mengambil nilai username dan password dari argumen yang diberikan. Nilai username diambil dari ``argv[2]`` dan password diambil dari ``argv[4]``. Ini dilakukan dengan menginisialisasi pointer ``username`` dan ``password`` untuk menunjuk ke argumen yang sesuai.

4. Selanjutnya, fungsi ini melakukan koneksi soket ke server dengan memanggil fungsi ``connect_socket()``. Hasil koneksi soket disimpan dalam variabel ``sock``.

5. Setelah itu, fungsi ini membangun pesan ``authenticate`` untuk otentikasi. Jika ``username`` sama dengan "root", maka pesan otentikasi diatur sebagai "authenticate root superuser". Jika bukan, maka pesan otentikasi diatur sebagai "authenticate [username] [password]". Ini dilakukan dengan menggunakan fungsi ``sprintf()`` untuk memformat pesan otentikasi ke dalam string authenticate.

6. Selanjutnya, pesan otentikasi dikirim ke server menggunakan fungsi ``send()``. Pesan tersebut dikirim melalui soket ``sock``, dengan panjang pesan yang dihitung menggunakan ``strlen(authenticate)``, dan flag 0.

7. Selanjutnya, program memasuki loop tak terbatas ``while(1)``. Di dalam loop ini, program menerima input perintah dari pengguna melalui fungsi ``fgets``(), yang menyimpan input ke dalam string ``command``. Kemudian, program mencetak pesan "Command from client: [command]" menggunakan printf(). Selanjutnya, perintah dikirim ke server menggunakan fungsi ``send()`` dengan menggunakan soket ``sock``, panjang pesan yang dihitung menggunakan ``strlen(command)``, dan flag 0.

8. Loop akan terus berlanjut sehingga program terus menerima dan mengirim perintah ke server sampai program dihentikan secara manual.

9. Setelah program keluar dari loop, soket ``sock`` ditutup menggunakan fungsi ``close(sock)``.

10. Terakhir, fungsi ``main()`` mengembalikan nilai 0 untuk menandakan bahwa program selesai dijalankan tanpa masalah.

# Penjelasan ``database``
Kode tersebut adalah implementasi program server database sederhana dalam bahasa C. Program ini mencakup beberapa fungsi dasar seperti autentikasi pengguna, pembuatan pengguna baru, pembuatan database, memberikan izin akses ke pengguna, dan beberapa operasi lainnya.

Berikut adalah beberapa komponen utama dari kode tersebut:

1. Header dan Libraries:

    - ``stdio.h``: Library standar untuk input/output dalam bahasa C.
    - ``stdlib.h``: Library standar untuk fungsi umum dalam bahasa C.
    - ``unistd.h``: Library untuk fungsi sistem POSIX.
    - ``string.h``: Library untuk manipulasi string.
    - ``stdbool.h``: Library untuk tipe data boolean.
    - ``dirent.h``: Library untuk membaca direktori.
    - ``sys/socket.h``: Library untuk operasi socket.
    - ``netinet/in.h``: Library untuk operasi jaringan Internet.
    - ``sys/stat.h``: Library untuk operasi status sistem.
    - ``sys/types.h``: Library untuk tipe data sistem.

2. Konstanta:

    - PORT: Port socket yang digunakan untuk komunikasi antara server dan klien.
    - LENGTH_OF_USERNAME: Panjang maksimum nama pengguna.
    - LENGTH_OF_PW: Panjang maksimum kata sandi.
    - DirectoryToDatabase: Direktori yang berisi database yang dibuat.
    - DirectoryToLibrary: Direktori yang berisi file-file konfigurasi dan akses pengguna.
    - current_db: Variabel untuk menyimpan nama database yang sedang digunakan.

3. Fungsi-Fungsi:

    - ``Authenticate``: Mengecek apakah kombinasi nama pengguna dan kata sandi terverifikasi.
    - ``User_Presence``: Mengecek apakah pengguna dengan nama pengguna tertentu sudah ada.
    - ``InsertUser``: Menambahkan pengguna baru ke dalam file daftar pengguna.
    - ``Create_Database``: Membuat direktori baru untuk database.
    - ``Grant_Permission``: Memberikan izin akses ke pengguna untuk database tertentu.
    - ``Check_Permission``: Mengecek izin akses pengguna ke database tertentu.
    - ``Drop_DB``: Menghapus database beserta isinya.
    - ``CreateDB_Folder``: Membuat direktori-direktori yang diperlukan untuk menyimpan database dan file konfigurasi.
    - ``connect_socket``: Membuat socket server dan melakukan koneksi dengan klien.
    - ``main``: Fungsi utama program.

4. Implementasi utama:

    - Pada fungsi ``main``, terdapat implementasi aliran kerja utama program.
    - Setelah memanggil fungsi ``CreateDB_Folder`` untuk membuat direktori-direktori yang diperlukan, program melakukan koneksi ke socket menggunakan fungsi ``connect_socket``.
    - Selanjutnya, program memulai membaca perintah yang diterima dari klien dan memprosesnya sesuai dengan tipe perintah yang dikenali.
    - Beberapa perintah yang dikenali antara lain: ``authenticate, create user, create database, use, grant permission, dan drop``.
    - Program berjalan dalam loop tak terbatas (``while (1)``) untuk terus menerima dan memproses perintah dari klien.

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <dirent.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/types.h>

// PORT socket di database harus sama dengan PORT socket di client
#define PORT 8085
#define LENGTH_OF_USERNAME 512
#define LENGTH_OF_PW 512
#define DirectoryToDatabase "databases"
#define DirectoryToLibrary "databases/lib"
char current_db[512] = "";
```
Header yang digunakan dalam kode tersebut:

``<stdio.h>``: Header ini berisi fungsi-fungsi standar untuk input/output dalam bahasa C.
``<stdlib.h>``: Header ini berisi fungsi-fungsi standar untuk alokasi memori, konversi tipe data, pengurutan, dan fungsi umum lainnya.
``<unistd.h>``: Header ini berisi fungsi-fungsi standar untuk sistem operasi, seperti ``read, write, close``, dan fungsi-fungsi lain yang terkait dengan sistem operasi.
``<string.h>``: Header ini berisi fungsi-fungsi standar untuk manipulasi string, seperti ``strcpy, strcmp, strtok``, dan sebagainya.
``<stdbool.h>``: Header ini berisi definisi tipe data bool yang memungkinkan penggunaan nilai ``boolean`` (``true atau false``) dalam bahasa C.
``<dirent.h>``: Header ini berisi fungsi-fungsi untuk bekerja dengan direktori, seperti membuka direktori, membaca isi direktori, dan sebagainya.
``<sys/socket.h>``: Header ini berisi fungsi-fungsi dan struktur data yang berkaitan dengan operasi socket, termasuk pembuatan, pengikatan, dan penerimaan koneksi pada socket.
``<netinet/in.h>``: Header ini berisi definisi struktur data dan konstanta untuk operasi jaringan, seperti alamat IP, port, dan tipe protokol.
``<sys/stat.h>``: Header ini berisi fungsi-fungsi untuk mendapatkan informasi tentang status file, seperti hak akses, ukuran, dan waktu modifikasi.
``<sys/types.h>``: Header ini berisi definisi tipe data dasar seperti ``size_t, pid_t, time_t``, dan sebagainya.

Variabel ``current_db`` adalah sebuah array karakter dengan ukuran 512 yang digunakan untuk menyimpan nama database saat ini yang sedang digunakan oleh program. Variabel ini akan diubah nilainya saat perintah "USE" dijalankan untuk mengganti konteks database yang aktif.

Selain itu, terdapat beberapa konstanta yang didefinisikan:

``PORT``: Konstanta ini menentukan nomor port yang digunakan untuk koneksi socket antara server dan client. Dalam kode ini, nilai yang digunakan adalah 8085.
``LENGTH_OF_USERNAME``: Konstanta ini menentukan panjang maksimum nama pengguna (username) yang dapat diterima, dalam jumlah karakter.
``LENGTH_OF_PW``: Konstanta ini menentukan panjang maksimum kata sandi (password) yang dapat diterima, dalam jumlah karakter.
``DirectoryToDatabase``: Konstanta ini menyimpan nama direktori tempat database disimpan.
``DirectoryToLibrary``: Konstanta ini menyimpan nama direktori tempat file-file terkait database disimpan, seperti file ``user_list.csv`` dan ``user_access.csv``.

```c
int Authenticate(char *username, char *password) {
    char user_list_dir[100];
    sprintf(user_list_dir, "%s/user_list.csv", DirectoryToLibrary);

    FILE *user_list = fopen(user_list_dir, "r");
    if (user_list != NULL) {
        char buf[256];
        char curr_usrname[LENGTH_OF_USERNAME];
        char curr_pw[LENGTH_OF_PW];

        while (fgets(buf, sizeof(buf), user_list)) {
            sscanf(buf, "%[^,],%s", curr_usrname, curr_pw); // ambil kolom pertama dan kolom kedua dari tiap barisnya.
            
            if (strcmp(curr_usrname, username) == 0 && strcmp(curr_pw, password) == 0) { // cek jika sama dengan input
                fclose(user_list);
                return 1; // Berhasil Masuk
            }
        }

        fclose(user_list);
    }
    return 0; // Gagal Masuk
}
```
Fungsi di atas adalah sebuah fungsi ``Authenticate`` yang digunakan untuk melakukan autentikasi pengguna berdasarkan username dan password yang diberikan. Berikut adalah penjelasan dari fungsi tersebut:

1. Membuat variabel ``user_list_dir`` yang merupakan array karakter dengan ukuran 100. Variabel ini digunakan untuk menyimpan path atau direktori ke file ``user_list.csv`` yang berada di dalam direktori DirectoryToLibrary. Fungsi ``sprintf`` digunakan untuk menggabungkan string ``DirectoryToLibrary`` dengan string ``/user_list.csv`` dan hasilnya disimpan dalam ``user_list_dir``.

2. Membuka file ``user_list.csv`` dengan menggunakan fungsi ``fopen`` dan mode "r" (read). File ini berisi daftar username dan password yang akan digunakan untuk autentikasi. Hasil dari operasi ``fopen`` disimpan dalam variabel ``user_list``.

3. Mengecek apakah ``user_list`` tidak kosong (``user_list != NULL``). Jika file berhasil dibuka, maka blok kode di dalamnya akan dieksekusi. Jika file gagal dibuka, fungsi akan langsung mengembalikan nilai 0, yang berarti autentikasi gagal.

4. Dalam blok kode yang dieksekusi jika file ``user_list`` berhasil dibuka, variabel ``buf`` digunakan sebagai buffer untuk membaca setiap baris dalam file ``user_list``. Variabel ``curr_usrname`` dan ``curr_pw`` digunakan untuk menyimpan username dan password saat ini yang diambil dari baris yang sedang dibaca.

5. Dalam loop ``while``, fungsi ``fgets`` digunakan untuk membaca baris dari file ``user_list`` dan menyimpannya dalam ``buf``. Kemudian, fungsi ``sscanf`` digunakan untuk memparsing buf dan mengambil username dan password dari kolom pertama dan kolom kedua dalam format ``%[^,],%s``. Hasil parsing disimpan dalam ``curr_usrname`` dan ``curr_pw``.

6. Setelah mendapatkan ``curr_usrname`` dan ``curr_pw``, dilakukan pengecekan apakah ``curr_usrname`` dan ``curr_pw`` sama dengan username dan password yang diberikan sebagai input. Jika kedua nilai tersebut sama, artinya autentikasi berhasil, dan fungsi akan mengembalikan nilai 1, yang berarti autentikasi berhasil masuk.

7. Jika autentikasi tidak berhasil pada setiap baris dalam file ``user_list``, maka file ``user_list`` ditutup dengan fungsi fclose dan fungsi mengembalikan nilai 0, yang berarti autentikasi gagal masuk.

Dengan demikian, fungsi ``Authenticate`` digunakan untuk membaca file ``user_list.csv``, membandingkan username dan password yang diberikan dengan setiap baris dalam file, dan mengembalikan nilai yang menunjukkan keberhasilan atau kegagalan autentikasi.
```c
int User_Presence(char *username) {
    char user_list_dir[100];
    sprintf(user_list_dir, "%s/user_list.csv", DirectoryToLibrary);

    FILE *user_list = fopen(user_list_dir, "r");
    if (user_list != NULL) {
        char buf[256];
        char curr_usrname[LENGTH_OF_USERNAME];

        while (fgets(buf, sizeof(buf), user_list)) {
            sscanf(buf, "%[^,]", curr_usrname); // Pada file csv, cari kolom pertama, yaitu sebelum koma ditemukan.
            
            if (strcmp(curr_usrname, username) == 0) {
                fclose(user_list);
                return 1; // Username ditemukan
            }
        }
        fclose(user_list);
    }
    return 0; // Username tidak ditemukan
}
```
```c
void InsertUser(char *username, char *password) {
    char user_list_dir[100];
    sprintf(user_list_dir, "%s/user_list.csv", DirectoryToLibrary);

    FILE *user_list = fopen(user_list_dir, "a");
    if (user_list != NULL) {
        fprintf(user_list, "%s,%s\n", username, password);
        fclose(user_list);
        return;
    } else {
        perror("Gagal mendaftarkan user");
    }
}
```
Fungsi di atas adalah ``InsertUser``, yang digunakan untuk menyisipkan (insert) informasi pengguna baru ke dalam file ``user_list.csv``. Berikut adalah penjelasan dari kode tersebut:

1. Membuat variabel ``user_list_dir`` yang merupakan array karakter dengan ukuran 100. Variabel ini digunakan untuk menyimpan path atau direktori ke file ``user_list.csv`` yang berada di dalam direktori ``DirectoryToLibrary``. Fungsi ``sprintf`` digunakan untuk menggabungkan string ``DirectoryToLibrary`` dengan string ``/user_list.csv`` dan hasilnya disimpan dalam ``user_list_dir``.

2. Membuka file ``user_list.csv`` dengan menggunakan fungsi ``fopen`` dan mode "a" (append). Mode "a" digunakan untuk membuka file dalam mode tambahan, yang berarti jika file sudah ada, penulisan data baru akan dilakukan pada akhir file. Jika file belum ada, file akan dibuat baru. Hasil dari operasi ``fopen`` disimpan dalam variabel ``user_list``.

3. Mengecek apakah ``user_list`` tidak kosong (``user_list != NULL``). Jika file berhasil dibuka, maka blok kode di dalamnya akan dieksekusi. Jika file gagal dibuka, fungsi akan menampilkan pesan kesalahan menggunakan fungsi ``perror`` dan fungsi ``InsertUser`` akan berhenti.

4. Dalam blok kode yang dieksekusi jika file ``user_list`` berhasil dibuka, menggunakan fungsi ``fprintf`` untuk menulis username dan password yang diberikan ke dalam file ``user_list``. Format ``%s,%s\n`` digunakan untuk menuliskan username dan password dalam format yang dipisahkan oleh koma (",") dan diikuti oleh karakter baru (\n) untuk memindahkan penulisan ke baris berikutnya.

5. Setelah selesai menulis, file ``user_list`` ditutup dengan fungsi ``fclose`` dan fungsi ``InsertUser`` akan berhenti.

6. Jika file ``user_list`` gagal dibuka, fungsi ``perror`` digunakan untuk menampilkan pesan kesalahan "Gagal mendaftarkan user".

Dengan demikian, fungsi ``InsertUser`` digunakan untuk menambahkan informasi pengguna baru ke dalam file user_list.csv dengan format username dan password yang dipisahkan oleh koma (",").

```c
int Create_Database(char *dbname) {
    char dbdir[100];
    sprintf(dbdir, "%s/%s", DirectoryToDatabase, dbname);

    int created = mkdir(dbdir, 0777);
    if (created == 0) {
        printf("Database %s berhasil dibuat\n", dbname);
        return 1; 
    } else {
        perror("Gagal membuat Database");
        return 0; 
    }
}
```
Fungsi di atas adalah Create_Database, yang digunakan untuk membuat direktori baru yang akan menjadi database dengan nama yang diberikan. Berikut adalah penjelasan dari kode tersebut:

1. Membuat variabel ``dbdir`` yang merupakan array karakter dengan ukuran 100. Variabel ini digunakan untuk menyimpan path atau direktori ke database baru yang akan dibuat. Fungsi ``sprintf`` digunakan untuk menggabungkan string ``DirectoryToDatabase`` (direktori utama) dengan string ``dbname`` (nama database) dan hasilnya disimpan dalam ``dbdir``.

2. Menggunakan fungsi ``mkdir`` untuk membuat direktori baru dengan nama yang dihasilkan dalam variabel ``dbdir``. Mode ``0777`` digunakan untuk memberikan hak akses penuh (read, write, dan execute) pada direktori yang dibuat. Hasil dari operasi ``mkdir`` disimpan dalam variabel ``created``.

3. Mengecek apakah ``created`` bernilai 0. Jika nilai ``created`` adalah 0, itu berarti direktori berhasil dibuat. Maka blok kode di dalamnya akan dieksekusi.

    - Pesan "Database {nama database} berhasil dibuat" ditampilkan menggunakan ``printf``, dengan menggunakan nilai ``dbname`` yang diberikan sebagai argumen untuk mencetak nama database yang berhasil dibuat.
    - Fungsi ``Create_Database`` mengembalikan nilai 1 untuk menunjukkan bahwa pembuatan database berhasil.

4. Jika ``created`` bukan 0, itu berarti pembuatan direktori gagal. Blok kode yang sesuai di dalam ``else``dieksekusi.

    - Pesan kesalahan "Gagal membuat Database" ditampilkan menggunakan fungsi ``perror`` yang menampilkan pesan yang lebih rinci tentang kesalahan yang terjadi.
    - Fungsi ``Create_Database`` mengembalikan nilai 0 untuk menunjukkan bahwa pembuatan database gagal.

Dengan demikian, fungsi ``Create_Database`` digunakan untuk membuat direktori baru sebagai database dengan menggunakan nama yang diberikan. Jika pembuatan direktori berhasil, fungsi mengembalikan nilai 1. Jika pembuatan direktori gagal, fungsi mengembalikan nilai 0 dan menampilkan pesan kesalahan.

```c
int Grant_Permission(char *username, char *dbname) {
    char dbdir[100];
    sprintf(dbdir, "%s/%s", DirectoryToDatabase, dbname);

    DIR* dir = opendir(dbdir);
    if (dir) {
        closedir(dir);
    } else {
        printf("Database tidak tersedia\n");
        return 1;
    }

    char user_access_dir[100];
    sprintf(user_access_dir, "%s/user_access.csv", DirectoryToLibrary);

    FILE *user_access = fopen(user_access_dir, "a");
    if (user_access != NULL) {
        fprintf(user_access, "%s,%s\n", username, dbname);
        fclose(user_access);
        printf("Berhasil memberikan permission user %s ke database %s\n", username, dbname);
        return 0;
    } else {
        printf("Gagal memberikan permission");
        return 1;        
    }
}
```
Fungsi di atas adalah ``Grant_Permission``, yang digunakan untuk memberikan izin akses kepada pengguna (``username``) ke dalam sebuah database (``dbname``). Berikut adalah penjelasan dari kode tersebut:

1. Membuat variabel ``dbdir`` yang merupakan array karakter dengan ukuran 100. Variabel ini digunakan untuk menyimpan path atau direktori ke database yang akan diberikan izin akses. Fungsi ``sprintf`` digunakan untuk menggabungkan string ``DirectoryToDatabase`` (direktori utama database) dengan string ``dbname`` (nama database) dan hasilnya disimpan dalam ``dbdir``.

2. Mengecek apakah direktori dengan path yang disimpan dalam ``dbdir`` dapat dibuka dengan menggunakan fungsi ``opendir``. Jika direktori dapat dibuka, berarti database tersedia dan blok kode di dalamnya tidak dieksekusi. Jika direktori tidak dapat dibuka, itu berarti database tidak tersedia, dan pesan "Database tidak tersedia" dicetak menggunakan ``printf``. Fungsi ``Grant_Permission`` mengembalikan nilai 1 untuk menunjukkan bahwa database tidak tersedia.

3. Membuat variabel ``user_access_dir`` yang merupakan array karakter dengan ukuran 100. Variabel ini digunakan untuk menyimpan path atau direktori ke file ``user_access.csv``, yang akan digunakan untuk mencatat izin akses pengguna ke database. Fungsi ``sprintf`` digunakan untuk menggabungkan string ``DirectoryToLibrary`` (direktori utama library) dengan string ``user_access.csv`` dan hasilnya disimpan dalam ``user_access_dir``.

4. Membuka file ``user_access.csv`` dalam mode "a" (append) menggunakan fungsi ``fopen``. Jika file berhasil dibuka, blok kode di dalamnya akan dieksekusi.

    - Menggunakan ``fprintf`` untuk menulis username dan dbname ke dalam file ``user_access.csv`` dalam format "username,dbname".
    - Menutup file menggunakan ``fclose``.
    - Pesan "Berhasil memberikan permission user {username} ke database {dbname}" dicetak menggunakan ``printf``, dengan menggunakan nilai ``username`` dan ``dbname`` yang diberikan sebagai argumen untuk mencetak informasi yang tepat.
    - Fungsi ``Grant_Permission`` mengembalikan nilai 0 untuk menunjukkan bahwa izin akses berhasil diberikan.

5. Jika file ``user_access.csv`` tidak dapat dibuka, blok kode yang sesuai di dalam ``else`` dieksekusi.

    - Pesan kesalahan "Gagal memberikan permission" dicetak menggunakan ``printf``.
    - Fungsi ``Grant_Permission`` mengembalikan nilai 1 untuk menunjukkan bahwa gagal memberikan izin akses.

Dengan demikian, fungsi ``Grant_Permission`` digunakan untuk memberikan izin akses pengguna ke database. Jika database tidak tersedia atau gagal membuka file ``user_access.csv``, fungsi mengembalikan nilai 1 dan mencetak pesan kesalahan. Jika izin akses berhasil diberikan, fungsi mengembalikan nilai 0 dan mencetak pesan sukses.

```c
int Check_Permission(char *username, char *dbname) {
    char user_access_dir[100];
    sprintf(user_access_dir, "%s/user_access.csv", DirectoryToLibrary);

    FILE *user_access = fopen(user_access_dir, "r");
    if (user_access != NULL) {
        char buf[512];
        char curr_usrname[LENGTH_OF_USERNAME];
        char curr_db[512];

        while (fgets(buf, sizeof(buf), user_access)) {
            sscanf(buf, "%[^,],%s", curr_usrname, curr_db); // ambil setiap username dan db_name dari setiap line

            if (strcmp(curr_usrname, username) == 0 && strcmp(curr_db, dbname) == 0) {
                fclose(user_access);
                return 1; 
            }
        }

        fclose(user_access);
    }

    return 0; // User does not have access rights to the database
}
```
Fungsi di atas adalah ``Check_Permission``, yang digunakan untuk memeriksa apakah pengguna (``username``) memiliki izin akses ke database (``dbname``). Berikut adalah penjelasan dari kode tersebut:

1. Membuat variabel ``user_access_dir`` yang merupakan array karakter dengan ukuran 100. Variabel ini digunakan untuk menyimpan path atau direktori ke ``file user_access.csv``, yang berisi informasi izin akses pengguna ke database. Fungsi ``sprintf`` digunakan untuk menggabungkan string ``DirectoryToLibrary`` (direktori utama library) dengan string ``user_access.csv`` dan hasilnya disimpan dalam ``user_access_dir``.

2. Membuka file ``user_access.csv`` dalam mode "r" (read) menggunakan fungsi ``fopen``. Jika file berhasil dibuka, blok kode di dalamnya akan dieksekusi.

    - Membuat variabel ``buf`` yang merupakan array karakter dengan ukuran 512. Variabel ini digunakan untuk menyimpan baris yang dibaca dari file.

    - Membuat variabel ``curr_usrname`` yang merupakan array karakter dengan ukuran ``LENGTH_OF_USERNAME``. Variabel ini digunakan untuk menyimpan username yang dibaca dari setiap baris.

    - Membuat variabel ``curr_db`` yang merupakan array karakter dengan ukuran 512. Variabel ini digunakan untuk menyimpan nama database yang dibaca dari setiap baris.

    - Menggunakan ``fgets`` untuk membaca baris dari file ``user_access.csv`` dan menyimpannya dalam buf.

    - Menggunakan ``sscanf`` untuk mem-parsing buf dan memisahkan username dan dbname menggunakan format "%[^,],%s". Hasilnya disimpan dalam ``curr_usrname`` dan ``curr_db``.

    - Memeriksa apakah ``curr_usrname`` sama dengan username dan ``curr_db`` sama dengan dbname. Jika keduanya sama, itu berarti pengguna memiliki izin akses ke database, dan blok kode di dalam if dieksekusi.

        - Menutup file menggunakan ``fclose``.

        - Fungsi ``Check_Permission`` mengembalikan nilai 1 untuk menunjukkan bahwa pengguna memiliki izin akses.

    - Menutup file menggunakan fclose.

3. Jika file ``user_access.csv`` tidak dapat dibuka, blok kode yang sesuai di dalam ``else`` dieksekusi.

4. Fungsi ``Check_Permission`` mengembalikan nilai 0 untuk menunjukkan bahwa pengguna tidak memiliki izin akses ke database.

Dengan demikian, fungsi ``Check_Permission`` digunakan untuk memeriksa izin akses pengguna ke database. Jika pengguna memiliki izin akses (ditemukan baris yang sesuai dengan ``username``dan ``dbname`` dalam file ``user_access.csv``), fungsi mengembalikan nilai 1. Jika tidak ditemukan izin akses atau gagal membuka file ``user_access.csv``, fungsi mengembalikan nilai 0.

```c
int Drop_DB(char *dbname){
    char dbdir[512];
    sprintf(dbdir, "%s/%s", DirectoryToDatabase, dbname);
    
    char cmd[1024];
    sprintf(cmd, "rm -rf %s", dbdir);
    int status = system(cmd);
    if (status == 0) {
        printf("Berhasil untuk remove database %s\n", dbname);
    } else {
        perror("Gagal untuk remove database");
        
    }
}
```
Fungsi di atas adalah ``Drop_DB``, yang digunakan untuk menghapus sebuah database (``dbname``). Berikut adalah penjelasan dari kode tersebut:

1. Membuat variabel ``dbdir`` yang merupakan array karakter dengan ukuran 512. Variabel ini digunakan untuk menyimpan path atau direktori ke database yang akan dihapus. Fungsi ``sprintf`` digunakan untuk menggabungkan string ``DirectoryToDatabase`` (direktori utama database) dengan string ``dbname`` dan hasilnya disimpan dalam ``dbdir``.

2. Membuat variabel ``cmd`` yang merupakan array karakter dengan ukuran 1024. Variabel ini digunakan untuk menyimpan perintah sistem (command) yang akan dijalankan untuk menghapus database. Fungsi ``sprintf`` digunakan untuk memformat perintah "rm -rf %s", di mana ``%s`` akan diganti dengan nilai dari ``dbdir``, dan hasilnya disimpan dalam ``cmd``.

3. Menggunakan fungsi ``system`` untuk menjalankan perintah sistem yang disimpan dalam ``cmd``. Perintah ``rm -rf`` digunakan untuk menghapus direktori secara rekursif dan paksa tanpa konfirmasi. Nilai yang dikembalikan oleh ``system`` akan menyimpan status hasil eksekusi perintah tersebut.

4. Memeriksa nilai ``status`` yang dikembalikan oleh ``system``. Jika ``status`` sama dengan 0, itu berarti perintah untuk menghapus database berhasil dieksekusi.

   - Mencetak pesan "Berhasil untuk remove database ``dbname``".

5. Jika ``status`` tidak sama dengan 0, berarti terjadi kesalahan saat menjalankan perintah untuk menghapus database.

   - Mencetak pesan kesalahan menggunakan ``perror``, dengan pesan "Gagal untuk remove database".

Dalam fungsi ini, database akan dihapus dengan menggunakan perintah sistem ``rm -rf`` yang dijalankan melalui fungsi ``system``. Jika penghapusan berhasil, pesan berhasil dicetak. Jika terjadi kesalahan, pesan kesalahan dicetak menggunakan ``perror``.

```c
void CreateDB_Folder(){
    // buat folder database (menyimpan database yang dibuat dalam bentuk .csv)
    int dbdir = mkdir(DirectoryToDatabase, 0777); // read write execute for everyone
    
    // buat folder akses database (menyimpan user_list.csv dan user_access.csv)
    int accessdir = mkdir(DirectoryToLibrary, 0700);
    
    // buat csv yang menyimpan kredensial user dan password
    char user_list_dir[100];
    sprintf(user_list_dir, "%s/user_list.csv", DirectoryToLibrary);

    FILE *user_list = fopen(user_list_dir, "a");
    if (user_list == NULL) {
        perror("Gagal membuat file user_list.csv");
        exit(EXIT_FAILURE);
    }
    fclose(user_list);

    // buat csv yang menyimpan akses dari user-user terdaftar
    char user_access_dir[100];
    sprintf(user_access_dir, "%s/user_access.csv", DirectoryToLibrary);

    FILE *user_access = fopen(user_access_dir, "a");
    if (user_access == NULL) {
        perror("Gagal membuat file user_access.csv");
        exit(EXIT_FAILURE);
    }
    fclose(user_access);
}
```
Fungsi di atas adalah ``CreateDB_Folder``, yang digunakan untuk membuat folder dan file yang diperlukan untuk menyimpan database dan akses pengguna. Berikut adalah penjelasan dari kode tersebut:

1. Membuat folder untuk menyimpan database dengan menggunakan fungsi `mkdir`. Fungsi ini menerima dua argumen, yaitu ``DirectoryToDatabase`` yang merupakan path atau direktori utama untuk database, dan mode ``0777`` yang memberikan hak baca, tulis, dan eksekusi kepada semua pengguna.

   - Nilai yang dikembalikan oleh ``mkdir`` disimpan dalam variabel ``dbdir``.

2. Membuat folder untuk menyimpan akses pengguna dengan menggunakan fungsi ``mkdir``. Fungsi ini menerima dua argumen, yaitu ``DirectoryToLibrary`` yang merupakan path atau direktori utama untuk akses pengguna, dan mode `0700` yang memberikan hak baca, tulis, dan eksekusi hanya kepada pemilik folder.

   - Nilai yang dikembalikan oleh ``mkdir`` disimpan dalam variabel ``accessdir``.

3. Membuat file ``user_list.csv`` untuk menyimpan kredensial pengguna. Fungsi ``sprintf`` digunakan untuk menggabungkan string `DirectoryToLibrary` dengan string ``"user_list.csv"``, dan hasilnya disimpan dalam ``user_list_dir``.

4. Menggunakan fungsi ``fopen`` untuk membuka file ``user_list.csv`` dengan mode ``"a"``, yang berarti file akan dibuka untuk ditambahkan (append). Jika ``fopen`` mengembalikan nilai ``NULL``, itu berarti gagal membuka file.

   - Jika gagal membuka file, pesan kesalahan dicetak menggunakan ``perror`` dengan pesan "Gagal membuat file user_list.csv".
   - Menggunakan ``exit(EXIT_FAILURE)`` untuk keluar dari program dengan status kegagalan.
   - Jika berhasil membuka file, file ditutup menggunakan ``fclose``.

5. Membuat file ``user_access.csv`` untuk menyimpan akses dari pengguna terdaftar. Fungsi ``sprintf`` digunakan untuk menggabungkan string `DirectoryToLibrary` dengan string ``"user_access.csv"``, dan hasilnya disimpan dalam ``user_access_dir``.

6. Menggunakan fungsi ``fopen`` untuk membuka file ``user_access.csv`` dengan mode `"a"`, yang berarti file akan dibuka untuk ditambahkan (append). Jika ``fopen`` mengembalikan nilai ``NULL``, itu berarti gagal membuka file.

   - Jika gagal membuka file, pesan kesalahan dicetak menggunakan ``perror`` dengan pesan "Gagal membuat file user_access.csv".
   - Menggunakan ``exit(EXIT_FAILURE)`` untuk keluar dari program dengan status kegagalan.
   - Jika berhasil membuka file, file ditutup menggunakan ``fclose``.

Dalam fungsi ini, folder dan file yang diperlukan untuk menyimpan database dan akses pengguna akan dibuat menggunakan ``mkdir``. Selanjutnya, file ``user_list.csv`` dan ``user_access.csv`` akan dibuat dalam folder akses pengguna menggunakan ``fopen``. Jika terjadi kesalahan saat membuat folder atau file, pesan kesalahan akan dicetak dan program akan keluar dengan status kegagalan menggunakan ``exit(EXIT_FAILURE)``.

```c
int connect_socket(){
    // Connect ke socket agar terhubung nantinya dengan client
    int socker_serv, accept_socket;
    struct sockaddr_in socket_addr;

    int addrlen = sizeof(socket_addr);
    
    if ((socker_serv = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("Gagal membuat socket server");
        exit(EXIT_FAILURE);
    }

    socket_addr.sin_port = htons(PORT);
    socket_addr.sin_family = AF_INET;
    socket_addr.sin_addr.s_addr = INADDR_ANY;
    
    if (bind(socker_serv, (struct sockaddr *)&socket_addr, sizeof(socket_addr)) < 0) {
        perror("Gagal melakukan bind ke socket");
        exit(EXIT_FAILURE);
    }

    if (listen(socker_serv, 3) < 0) {
        perror("Gagal melakukan listen ke socket");
        exit(EXIT_FAILURE);
    }

    printf("Server Database Siap Digunakan\n");

    if ((accept_socket = accept(socker_serv, (struct sockaddr *)&socket_addr, (socklen_t*)&addrlen)) < 0) {
        perror("Gagal melakukan accept ke socket");
        exit(EXIT_FAILURE);
    }

    return accept_socket;
}
```
Fungsi di atas adalah ``connect_socket``, yang digunakan untuk membuat dan menghubungkan socket server. Berikut adalah penjelasan dari kode tersebut:

1. Mendeklarasikan variabel ``socker_serv`` dan ``accept_socket`` dengan tipe data ``int``. ``socker_serv`` akan digunakan sebagai file descriptor untuk socket server, sedangkan ``accept_socket`` akan digunakan sebagai file descriptor untuk socket klien yang terhubung.

2. Mendeklarasikan variabel ``socket_addr`` dengan tipe data ``struct sockaddr_in``. Struktur ini digunakan untuk menyimpan informasi alamat socket, seperti alamat IP dan nomor port.

3. Mendeklarasikan variabel ``addrlen`` yang akan digunakan untuk menyimpan ukuran dari ``socket_addr``.

4. Membuat socket menggunakan fungsi ``socket``dengan parameter ``AF_INET`` untuk domain alamat (IPv4), ``SOCK_STREAM`` untuk tipe socket (TCP), dan `0` untuk protokol default. Jika pembuatan socket gagal, maka pesan kesalahan akan dicetak menggunakan ``perror`` dengan pesan "Gagal membuat socket server".

   a. Jika gagal membuat socket, program akan keluar dengan status kegagalan menggunakan ``exit(EXIT_FAILURE)``.

5. Mengisi informasi alamat socket pada variabel ``socket_addr``. Menetapkan nomor port menggunakan ``htons(PORT)`` untuk mengubah urutan byte agar sesuai dengan network byte order. Menetapkan jenis alamat menggunakan ``AF_INET`` untuk alamat IPv4. Menetapkan alamat IP menggunakan `INADDR_ANY` untuk mengikat socket ke semua antarmuka yang tersedia pada mesin.

6. Melakukan bind (pengikatan) socket server dengan alamat yang telah ditentukan menggunakan fungsi ``bind``. Jika pengikatan gagal, maka pesan kesalahan akan dicetak menggunakan ``perror`` dengan pesan "Gagal melakukan bind ke socket".

   a. Jika gagal melakukan bind, program akan keluar dengan status kegagalan menggunakan ``exit(EXIT_FAILURE)``.

7. Mengaktifkan socket server untuk menerima koneksi menggunakan fungsi ``listen``. Parameter ``3`` menentukan jumlah maksimum koneksi yang dapat diterima oleh server. Jika gagal melakukan listen, maka pesan kesalahan akan dicetak menggunakan ``perror`` dengan pesan "Gagal melakukan listen ke socket".

   a. Jika gagal melakukan listen, program akan keluar dengan status kegagalan menggunakan ``exit(EXIT_FAILURE)``.

8. Mencetak pesan "Server Database Siap Digunakan" untuk menandakan bahwa server telah siap menerima koneksi dari klien.

9. Menerima koneksi dari klien menggunakan fungsi ``accept``. Jika koneksi gagal, maka pesan kesalahan akan dicetak menggunakan `perror` dengan pesan "Gagal melakukan accept ke socket".

   a. Jika gagal melakukan accept, program akan keluar dengan status kegagalan menggunakan ``exit(EXIT_FAILURE)``.

10. Mengembalikan nilai ``accept_socket`` yang merupakan file descriptor untuk socket klien yang terhubung.

Fungsi ``connect_socket`` ini digunakan untuk membuat socket server, mengikatnya ke alamat yang ditentukan, melakukan listen untuk menerima koneksi, dan akhirnya menerima koneksi dari klien. Jika terjadi kesalahan pada salah satu tahapan tersebut, pesan kesalahan akan dicetak dan program akan keluar dengan status kegagalan menggunakan ``exit(EXIT_FAILURE)``.
```c
int main() {
    CreateDB_Folder(); // buat folder keperluan database
    int accept_socket = connect_socket(); // connect ke socket agar terhubung dengan client nantinya
    
    char username[512], password[512];
    char retrieveCommand[512];

    while (1){
        memset(retrieveCommand, 0, sizeof(retrieveCommand));
        int read_socket = read(accept_socket, retrieveCommand, sizeof(retrieveCommand) - 1); // ambil argumen yang ada di socket, simpan ke retrieveCommand

        char command[1024], rest_arg[1024];
        strcpy(command, retrieveCommand); // string menyimpan tipe command nya apa (authenticate, create user, dsbg.)
        strcpy(rest_arg, retrieveCommand); // string yang menyimpan sisa argumen diluar command (username, password, dsbg.)

        if (strstr(command, "authenticate") != NULL){ // command autentikasi (masuk sebagai user apa)
            char *tokenize = strtok(rest_arg, " "); // penggal kata pertama (authenticate)
            tokenize = strtok(NULL, " "); // ambil kata kedua (username)
            strcpy(username, tokenize);
            tokenize = strtok(NULL, " "); // ambil kata ketiga (password)
            strcpy(password, tokenize);

            if (password[strlen(password)-1] == '\n') password[strlen(password)-1] = '\0';

            int userRoot = strcmp(username, "root");
            int verified = Authenticate(username, password);

            if((userRoot == 0) || (verified == 1)){
                printf("Autentikasi Berhasil, Selamat Datang di Database %s!\n", username);
            }else{
                printf("Autentikasi Gagal, coba lagi!\n"); 
            }
        }

        else if (strstr(command, "CREATE USER") != NULL){ // command buat user baru
            int userRoot = strcmp(username, "root");

            if(userRoot == 0) { // hanya root yang bisa melakukan create user
                char new_user[512], new_pass[512];

                // Penggal kata CREATE dan USER
                char *tokenize = strtok(rest_arg, " ");
                tokenize = strtok(NULL, " ");

                // Ambil username
                tokenize = strtok(NULL, " ");
                strcpy(new_user, tokenize);
                
                // Penggal kata IDENTIFIED BY
                tokenize = strtok(NULL, " ");
                tokenize = strtok(NULL, " ");
                // Ambil password
                tokenize = strtok(NULL, ";");
                strcpy(new_pass, tokenize);
                
                if (User_Presence(new_user)) {
                    printf("Username sudah pernah digunakan, coba lagi!\n");
                } else {
                    InsertUser(new_user, new_pass);
                    printf("Berhasil mendaftarkan user %s ke database.\n", new_user);
                }

            } else {
                printf("Gagal melakukan CREATE USER, coba lagi!\n");
            }

        } else if(strstr(command, "CREATE DATABASE") != NULL){ // command membuat database
            // Penggal kata CREATE dan DATABASE
			char *tokenize = strtok(rest_arg, " ");
        	tokenize = strtok(NULL, " ");

            // Ambil Nama Database
            char new_db[512];
        	tokenize = strtok(NULL, " ");
        	tokenize = strtok(tokenize, ";"); // remove semicolon
            strcpy(new_db, tokenize);

        	Create_Database(new_db);

            int userRoot = strcmp(username, "root");

            if(userRoot != 0){  
                Grant_Permission(username, new_db); // beri akses ke pembuat database
            } 

            Grant_Permission("root", new_db); // root otomatis memiliki akses terhadap database tersebut

		} else if(strstr(command, "USE") != NULL) { // command USE untuk mengakses database 
            // Penggal kata USE
        	char *tokenize = strtok(rest_arg, " "); 

            // Ambil [nama_db];
        	tokenize = strtok(NULL, " ");
            // Remove semicolon                                                                       
        	char *dbname = strtok(tokenize, ";");

            int userRoot = strcmp(username, "root");
            int have_permission = Check_Permission(username, dbname);

        	if (userRoot == 0 || have_permission == 1){ // jika root ATAU memiliki permission
        		strcpy(current_db, dbname); // ganti context database saat ini 
                printf("User %s berhasil akses ke database %s", username, dbname);
			} else {
				printf("Gagal mengakses database %s\n", dbname);
			}

		} else if(strstr(command, "GRANT PERMISSION")){ // command untuk memberi permission ke suatu user
            // Penggal kata GRANT dan PERMISSION
            char *tokenize = strtok(rest_arg, " ");
        	tokenize = strtok(NULL, " ");

            // Ambil nama user
            char new_user[512], dbname[512];
            tokenize = strtok(NULL, " ");
            strcpy(dbname, tokenize);
            // Penggal kata INTO
            tokenize = strtok(NULL, " ");
            // Ambil nama DB, hapus semicolon
            tokenize = strtok(NULL, ";");
            strcpy(new_user, tokenize);

            int userRoot = strcmp(username, "root");

            if ((userRoot == 0) && User_Presence(new_user)) {
                Grant_Permission(new_user, dbname);
            } else {
                printf("Gagal memberi permission\n");
            }
        } else if(strstr(command, "DROP") != NULL) { // command drop untuk hapus
            if(strstr(command, "DATABASE") != NULL){ // hapus database
                char dbname[512];

                // Penggal kata DROP dan DATABASE
                char *tokenize = strtok(rest_arg, " ");
                tokenize = strtok(NULL, " ");

                // Ambil nama database dan remove semicolon
                tokenize = strtok(NULL, ";");
                strcpy(dbname, tokenize);

                int have_permission = Check_Permission(username, dbname);

                if (have_permission == 1) {
                    Drop_DB(dbname);
                } else {
                    printf("User %s tidak memiliki akses ke database %s\n", username, dbname);
                }
            }
            
        } else printf("Command tidak valid, coba lagi!");
    }
    
    return 0;
}
```

# Penjelasan Fungsi Main()
Kode dari ``database.c`` adalah program dari database server yang nantinya akan digunakan oleh client, mereka saling terhubung melalui soket yang digunakan. Berikut adalah ringkasan penjelasan kode tersebut:

## Fungsi Autentikasi User
```c
 if (strstr(command, "authenticate") != NULL){ // command autentikasi (masuk sebagai user apa)
            char *tokenize = strtok(rest_arg, " "); // penggal kata pertama (authenticate)
            tokenize = strtok(NULL, " "); // ambil kata kedua (username)
            strcpy(username, tokenize);
            tokenize = strtok(NULL, " "); // ambil kata ketiga (password)
            strcpy(password, tokenize);

            if (password[strlen(password)-1] == '\n') password[strlen(password)-1] = '\0';

            int userRoot = strcmp(username, "root");
            int verified = Authenticate(username, password);

            if((userRoot == 0) || (verified == 1)){
                printf("Autentikasi Berhasil, Selamat Datang di Database %s!\n", username);
            }else{
                printf("Autentikasi Gagal, coba lagi!\n"); 
            }
        }
```
Pada kode tersebut, kita akan memeriksa command yang dikirim dari client yaitu authenticate -u [name] -p [password], nantinya kita akan memenggal masing-masing username dan password dengan fungsi ``strtok()`` kemudian kita akan memeriksa file ``user_list.csv`` apakah user dan password itu bersesuaian / sudah terdaftar.

## Penjelasan Create user
```c
 else if (strstr(command, "CREATE USER") != NULL){ // command buat user baru
            int userRoot = strcmp(username, "root");

            if(userRoot == 0) { // hanya root yang bisa melakukan create user
                char new_user[512], new_pass[512];

                // Penggal kata CREATE dan USER
                char *tokenize = strtok(rest_arg, " ");
                tokenize = strtok(NULL, " ");

                // Ambil username
                tokenize = strtok(NULL, " ");
                strcpy(new_user, tokenize);
                
                // Penggal kata IDENTIFIED BY
                tokenize = strtok(NULL, " ");
                tokenize = strtok(NULL, " ");
                // Ambil password
                tokenize = strtok(NULL, ";");
                strcpy(new_pass, tokenize);
                
                if (User_Presence(new_user)) {
                    printf("Username sudah pernah digunakan, coba lagi!\n");
                } else {
                    InsertUser(new_user, new_pass);
                    printf("Berhasil mendaftarkan user %s ke database.\n", new_user);
                }

            } else {
                printf("Gagal melakukan CREATE USER, coba lagi!\n");
            }
```

Sama halnya dengan proses autentikasi, pada proses create user, kita akan memenggal command yang dikirim client secara per-kata. Nantinya dari command CREATE USER [nama] IDENTIFIED BY [pw] kita akan mengambil username dan passwordnya, lalu kita panggil fungsi ``User_Presence()`` untuk memeriksa apakah user sudah terdaftar, jika belum maka panggil fungsi ``InsertUser()``.

## Penjelasan CREATE DATABASE
```c
 else if(strstr(command, "CREATE DATABASE") != NULL){ // command membuat database
            // Penggal kata CREATE dan DATABASE
			char *tokenize = strtok(rest_arg, " ");
        	tokenize = strtok(NULL, " ");

            // Ambil Nama Database
            char new_db[512];
        	tokenize = strtok(NULL, " ");
        	tokenize = strtok(tokenize, ";"); // remove semicolon
            strcpy(new_db, tokenize);

        	Create_Database(new_db);

            int userRoot = strcmp(username, "root");

            if(userRoot != 0){  
                Grant_Permission(username, new_db); // beri akses ke pembuat database
            } 

            Grant_Permission("root", new_db); // root otomatis memiliki akses terhadap database tersebut

		} 
```

Untuk command CREATE DATABASE, kita hanya perlu memenggal satu kata yaitu nama dari databasenya, kemiudian kita akan panggil fungsi ``Create_Database()`` dengan parameter yaitu nama databasenya. Nantinya fungsi tersebut akan membuat suatu folder baru yang kami anggap sebagai "database" yang mana isinya nanti adalah tabel-tabel entitasnya dalam bentuk .csv

Jika sudah, kita akan memberikan permission kepada root pada database tersebut dengan memanggil fungsi ``Grant_Permission()``. Nantinya, fungsi tersebut akan memodifikasi file ``user_access.csv`` untuk menambahkan root pada database tersebut.

## Penjelasan USE
```c
else if(strstr(command, "USE") != NULL) { // command USE untuk mengakses database 
            // Penggal kata USE
        	char *tokenize = strtok(rest_arg, " "); 

            // Ambil [nama_db];
        	tokenize = strtok(NULL, " ");
            // Remove semicolon                                                                       
        	char *dbname = strtok(tokenize, ";");

            int userRoot = strcmp(username, "root");
            int have_permission = Check_Permission(username, dbname);

        	if (userRoot == 0 || have_permission == 1){ // jika root ATAU memiliki permission
        		strcpy(current_db, dbname); // ganti context database saat ini 
                printf("User %s berhasil akses ke database %s", username, dbname);
			} else {
				printf("Gagal mengakses database %s\n", dbname);
			}
```

Selanjutnya, untuk command use kita perlu memenggal satu kata yaitu nama dari databasenya, sehingga nantinya kita bisa memanggil fungsi ``Check_Permission()`` untuk memeriksa apakah active user / user sekarang memiliki permission ke db atau tidak. Jika memiliki, maka kita akan mengganti context database sekarang yaitu dengan mengganti variabel dari ``current_db``

## Penjelasan Grant Permission
```c
 else if(strstr(command, "GRANT PERMISSION")){ // command untuk memberi permission ke suatu user
            // Penggal kata GRANT dan PERMISSION
            char *tokenize = strtok(rest_arg, " ");
        	tokenize = strtok(NULL, " ");

            // Ambil nama user
            char new_user[512], dbname[512];
            tokenize = strtok(NULL, " ");
            strcpy(dbname, tokenize);
            // Penggal kata INTO
            tokenize = strtok(NULL, " ");
            // Ambil nama DB, hapus semicolon
            tokenize = strtok(NULL, ";");
            strcpy(new_user, tokenize);

            int userRoot = strcmp(username, "root");

            if ((userRoot == 0) && User_Presence(new_user)) {
                Grant_Permission(new_user, dbname);
            } else {
                printf("Gagal memberi permission\n");
            }

```
Kemudian, untuk fungsi GRANT PERMISSION, kita akan memenggal 2 kata yaitu username dan nama databasenya, jika sudah maka kita hanya perlu memanggil fungsi ``Grant_Permission()`` lalu fungsi akan membuat list baru pada csv yaitu username yang telah diberikan akses ke database yang diberikan.

## Penjelasan DROP DATABASE
```c
else if(strstr(command, "DROP") != NULL) { // command drop untuk hapus
            if(strstr(command, "DATABASE") != NULL){ // hapus database
                char dbname[512];

                // Penggal kata DROP dan DATABASE
                char *tokenize = strtok(rest_arg, " ");
                tokenize = strtok(NULL, " ");

                // Ambil nama database dan remove semicolon
                tokenize = strtok(NULL, ";");
                strcpy(dbname, tokenize);

                int have_permission = Check_Permission(username, dbname);

                if (have_permission == 1) {
                    Drop_DB(dbname);
                } else {
                    printf("User %s tidak memiliki akses ke database %s\n", username, dbname);
                }
            }
            
        } else printf("Command tidak valid, coba lagi!");
    }

```
Untuk command terakhir, yaitu adalah DROP DATABASE. Kita perlu memenggal nama dari database yang dikirim client, jika sudah kita akan memeriksa apakah active user memiliki akses dengan cara memanggil fungsi ``Check_Permission()``. Jika IYA, maka kita langsung memanggil fungsi ``Drop_DB()`` untuk menghapus folder database yang dituju.

# Penjelasan ``cron``
Skrip `cron.sh` merupakan sebuah skrip yang digunakan dalam pengaturan Cron job. Cron job adalah sebuah utilitas pada sistem operasi Unix yang memungkinkan pengguna menjadwalkan eksekusi tugas-tugas secara otomatis pada waktu tertentu atau berulang.

Berikut adalah penjelasan dari isi skrip `cron.sh`:

1. `0 */1 * * *`: Ini adalah baris pertama dalam skrip `cron.sh` dan merupakan konfigurasi waktu untuk menjalankan Cron job. Pada baris ini, terdapat aturan untuk menjalankan skrip ini setiap jam pada menit ke-0. Dengan demikian, skrip ini akan dijalankan setiap jam.

2. `cd /home/dimasps32/sisop_praktikum_final/database`: Perintah ini mengubah direktori kerja saat ini ke `/home/dimasps32/sisop_praktikum_final/database`. Artinya, semua perintah yang dijalankan setelah ini akan dieksekusi dalam direktori tersebut.

3. `/usr/bin/zip -rm `date "+\%Y-\%m-\%d"`\ `date +"\%H:\%M:\%S"` dblog.log *.backup`: Ini adalah perintah yang akan dijalankan oleh Cron job.

   a. `/usr/bin/zip`: Ini adalah perintah untuk mengompres file dan folder menjadi file zip.

   b. `-rm`: Opsi `-r` digunakan untuk mengompres secara rekursif seluruh isi folder, sedangkan opsi `-m` digunakan untuk menghapus file sumber setelah dikompres.

   c. ``date "+\%Y-\%m-\%d"``: Ini adalah perintah yang digunakan untuk mendapatkan tanggal saat ini dengan format "YYYY-MM-DD". Tanggal ini akan digunakan sebagai bagian dari nama file zip.

   d. ``date +"\%H:\%M:\%S"``: Ini adalah perintah yang digunakan untuk mendapatkan waktu saat ini dengan format "HH:MM:SS". Waktu ini akan digunakan sebagai bagian dari nama file zip.

   e. `dblog.log`: Ini adalah nama file log yang akan dikompres.

   f. `*.backup`: Ini adalah pola untuk memilih semua file dengan ekstensi `.backup` dalam direktori saat ini.

Dengan demikian, skrip `cron.sh` akan menjalankan perintah untuk mengompres file dengan nama file zip yang mencakup tanggal dan waktu saat ini, serta menghapus file asli dengan ekstensi `.backup` dalam direktori `/home/dimasps32/sisop_praktikum_final/database`. Skrip ini akan dijalankan setiap jam sesuai dengan jadwal yang ditentukan pada baris pertama.

# Penjelasan ``dump``

``client_dump.c`` adalah sebuah program yang melakukan komunikasi socket dengan server database. Berikut adalah gambaran umum tentang apa yang dilakukan oleh kode tersebut:

1. Kode memulai dengan memeriksa validitas argumen yang diberikan saat menjalankan program. Jika argumen tidak valid, program akan mencetak pesan kesalahan dan keluar.

2. Selanjutnya, program membuat soket (socket) dan melakukan koneksi ke server database yang ditentukan. Jika koneksi gagal, program mencetak pesan kesalahan dan keluar.

3. Setelah terhubung ke server database, program mengirimkan perintah ke server untuk mengidentifikasi jenis akses dan tindakan yang akan dilakukan. Perintah ini dikirim melalui soket.

4. Program menerima respons dari server dan memeriksa apakah autentikasi berhasil. Jika autentikasi gagal, program mencetak pesan kesalahan dan keluar.

5. Selanjutnya, program mengirim perintah ke server untuk menggunakan database yang sesuai dengan argumen yang diberikan saat menjalankan program.

6. Program menerima respons dari server dan memeriksa apakah perubahan database berhasil. Jika perubahan database berhasil, program melakukan pemrosesan tambahan dan kemudian menutup koneksi soket sebelum berakhir.

7. Jika perubahan database tidak berhasil, program langsung menutup koneksi soket sebelum berakhir.

Kode juga menyertakan beberapa fungsi bantu untuk validasi argumen, membuat soket dan melakukan koneksi ke server, mengirim dan menerima data melalui soket, serta menangani perubahan database.


```c
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>

#define PORT 8085
```
Pada bagian awal, terdapat beberapa penggunaan ``#include`` untuk memasukkan beberapa file header ke dalam program. File header mengandung deklarasi dan definisi fungsi, tipe data, dan konstanta yang digunakan dalam kode. Berikut adalah penjelasan untuk setiap file header yang dimasukkan:

- ``#include <stdio.h>``: File header ini memasukkan fungsi-fungsi standar input/output, seperti printf dan scanf, yang digunakan untuk operasi dasar input/output.
- ``#include <sys/socket.h>``: File header ini menyediakan fungsi-fungsi dan struktur data untuk pemrograman socket, yang memungkinkan komunikasi antara proses-proses melalui jaringan.
- ``#include <stdlib.h>``: File header ini memasukkan fungsi-fungsi umum, seperti alokasi memori dan pengendalian proses, seperti malloc dan exit.
- ``#include <netinet/in.h>``: File header ini berisi konstanta-konstanta dan struktur data yang diperlukan untuk alamat domain internet, seperti struct sockaddr_in yang mewakili alamat internet.
- ``#include <string.h>``: File header ini menyediakan berbagai fungsi manipulasi string, seperti strcpy dan strlen, yang digunakan untuk bekerja dengan string.
- ``#include <unistd.h>``: File header ini memberikan akses ke berbagai fungsi API sistem operasi POSIX, termasuk manipulasi file dan pengelolaan proses, seperti close dan fork.
- ``#include <arpa/inet.h>``: File header ini berisi fungsi-fungsi untuk memanipulasi alamat IP dalam representasi byte urutan jaringan, seperti inet_addr dan inet_ntoa.
- ``#include <stdbool.h>``: File header ini menyediakan tipe data bool dan makro true dan false, yang digunakan untuk nilai boolean dalam bahasa C.

Selanjutnya, kode mendefinisikan konstanta bernama ``PORT`` dan memberikannya nilai ``8085``. Konstanta ini mewakili nomor port yang akan digunakan untuk komunikasi socket. Nomor port menentukan layanan atau aplikasi spesifik yang akan menerima data jaringan.

```c
bool isSocketClosed(int readValue, int socketDescriptor) {
    if (readValue == 0) {
        close(socketDescriptor);
        return true;
    }
    return false;
}
```
Kode tersebut adalah sebuah fungsi bernama ``isSocketClosed`` yang digunakan untuk memeriksa apakah soket (socket) telah ditutup atau tidak. Berikut adalah penjelasan lebih rinci mengenai kode tersebut:

1. Fungsi ``isSocketClosed`` memiliki dua parameter:

   - ``readValue``: Sebuah nilai integer yang merepresentasikan hasil pembacaan dari soket (socket). Nilai ini biasanya didapatkan dari fungsi ``recv`` yang mengembalikan jumlah byte yang berhasil dibaca dari soket.

   - ``socketDescriptor``: Sebuah nilai integer yang merupakan deskriptor soket (socket). Deskriptor soket ini digunakan untuk melakukan operasi terkait soket, seperti menutup soket dengan fungsi ``close``.

2. Pada baris pertama fungsi, dilakukan pengecekan apakah nilai ``readValue`` sama dengan 0. Jika kondisi ini terpenuhi, artinya tidak ada data yang berhasil dibaca dari soket dan soket tersebut dianggap telah ditutup.

3. Jika soket dianggap ditutup, maka dilakukan pemanggilan fungsi ``close`` dengan memberikan ``socketDescriptor`` sebagai argumen. Fungsi ``close`` digunakan untuk menutup soket dan melepaskan sumber daya yang terkait.

4. Setelah menutup soket, fungsi mengembalikan nilai ``true`` untuk menunjukkan bahwa soket telah ditutup.

5. Jika kondisi pada baris pertama tidak terpenuhi, artinya soket masih terbuka atau ada data yang berhasil dibaca. Pada baris terakhir fungsi, nilai ``false`` dikembalikan untuk menunjukkan bahwa soket belum ditutup.

Dengan demikian, fungsi ``isSocketClosed`` digunakan untuk memeriksa apakah soket telah ditutup atau belum berdasarkan nilai ``readValue`` yang diperoleh dari operasi pembacaan soket.

```c
bool validateArguments(int argc, char const *argv[]) {
    if (getuid()) {
        if (argc < 6 || (strcmp(argv[1], "-u") != 0) || (strcmp(argv[3], "-p") != 0)) {
            return false;
        }
    } else {
        if (argc < 2) {
            return false;
        }
    }
    return true;
}
```
Fungsi tersebut bernama ``validateArguments`` dan digunakan untuk memvalidasi argumen yang diberikan saat menjalankan program. Berikut adalah penjelasan lebih detail mengenai fungsi tersebut:

1. Fungsi ``validateArguments`` memiliki dua parameter:

    - ``argc``: Sebuah nilai integer yang merupakan jumlah argumen yang diberikan saat menjalankan program.
    - ``argv``: Sebuah array pointer dari ``const char``, yang berisi argumen-argumen yang diberikan saat menjalankan program.

2. Pada baris pertama fungsi, terdapat penggunaan fungsi ``getuid()``. Fungsi ini digunakan untuk mendapatkan user ID dari pengguna yang menjalankan program. Jika user ID bukan 0 (root user), artinya bukan root user.

3. Pada blok ``if`` pertama (untuk non-root user), dilakukan pengecekan kondisi sebagai berikut:

    - Jika ``argc`` kurang dari 2, artinya jumlah argumen yang diberikan kurang dari yang diharapkan. Dalam hal ini, fungsi mengembalikan nilai ``false untuk`` menandakan argumen tidak valid.

4. Pada blok ``else`` (untuk root user), dilakukan pengecekan kondisi yang lebih spesifik:

    - Jika ``argc`` kurang dari 6 (tidak ada cukup argumen yang diberikan), atau argumen pertama ``argv[1])`` tidak sama dengan ``"-u"``, atau argumen ketiga ``(argv[3])`` tidak sama dengan ``"-p"``, maka fungsi mengembalikan nilai ``false`` untuk menunjukkan argumen tidak valid.

5. Jika tidak ada kondisi yang terpenuhi pada kedua blok di atas, artinya argumen dinyatakan valid. Fungsi mengembalikan nilai ``true`` untuk menandakan argumen valid.

Dengan demikian, fungsi ``validateArguments`` digunakan untuk memeriksa apakah argumen yang diberikan saat menjalankan program sesuai dengan kondisi yang diharapkan. Fungsi ini memberikan nilai ``true`` jika argumen valid dan ``false`` jika tidak valid.

```c
int createSocketAndConnect(struct sockaddr_in *serverAddress) {
    int socketDescriptor;
    if ((socketDescriptor = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        return -1;
    }

    memset(serverAddress, '0', sizeof(*serverAddress));
    serverAddress->sin_family = AF_INET;
    serverAddress->sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &(serverAddress->sin_addr)) <= 0) {
        return -1;
    }

    if (connect(socketDescriptor, (struct sockaddr *)serverAddress, sizeof(*serverAddress)) < 0) {
        return -1;
    }

    return socketDescriptor;
}
```
Kode tersebut adalah fungsi dengan nama ``createSocketAndConnect`` yang digunakan untuk membuat soket (socket) dan melakukan koneksi ke server. Berikut adalah penjelasan lebih rinci tentang kode tersebut:

1. Fungsi ``createSocketAndConnect`` memiliki satu parameter:

- ``serverAddress``: Pointer ke struktur struct sockaddr_in yang akan digunakan untuk menyimpan informasi alamat server yang akan dikoneksikan.

2. Pada baris pertama fungsi, dideklarasikan variabel ``socketDescriptor`` yang akan digunakan sebagai deskriptor soket (socket) yang akan dibuat.

3. Pada baris ke-3, fungsi ``socket`` digunakan untuk membuat soket baru. Jika pemanggilan fungsi tersebut mengembalikan nilai negatif, artinya pembuatan soket gagal. Dalam hal ini, fungsi mengembalikan nilai -1 untuk menandakan kesalahan.

4. Pada baris ke-6, dilakukan inisialisasi nilai-nilai pada ``serverAddress``. Pertama, menggunakan fungsi ``memset`` untuk mengatur semua byte dalam ``serverAddress`` menjadi karakter ``'0'``. Kemudian, disetel nilai ``sin_family`` menjadi ``AF_INET`` untuk menunjukkan bahwa menggunakan protokol IPv4. Selanjutnya, ``sin_port`` disetel dengan menggunakan ``htons(PORT)`` untuk menentukan nomor port yang akan digunakan dalam koneksi soket.

5. Pada baris ke-10, fungsi ``inet_pton`` digunakan untuk mengonversi alamat IP dalam bentuk string menjadi representasi biner yang sesuai dengan ``sin_addr`` di ``serverAddress``. Jika konversi gagal (mengembalikan nilai negatif), fungsi mengembalikan nilai ``-1``.

6. Pada baris ke-13, fungsi ``connect`` digunakan untuk melakukan koneksi ke server dengan menggunakan soket yang telah dibuat sebelumnya dan alamat server yang ditentukan dalam ``serverAddress``. Jika koneksi gagal (mengembalikan nilai negatif), fungsi mengembalikan nilai ``-1``.

7. Jika seluruh langkah di atas berhasil dilakukan, fungsi mengembalikan ``socketDescriptor`` yang merupakan deskriptor soket yang telah terhubung ke server.

Dengan demikian, fungsi ``createSocketAndConnect`` digunakan untuk membuat soket baru, mengatur alamat server, melakukan koneksi soket ke server, dan mengembalikan deskriptor soket yang telah terhubung.

```c
void sendData(int socketDescriptor, const char *data) {
    send(socketDescriptor, data, strlen(data), 0);
}
```

Fungsi ``sendData`` digunakan untuk mengirim data melalui soket (socket). Berikut adalah penjelasan lebih rinci tentang fungsi tersebut:

1. Fungsi ``sendData`` memiliki dua parameter:

    - ``socketDescriptor``: Sebuah nilai integer yang merupakan deskriptor soket (socket) yang akan digunakan untuk mengirim data.
    - ``data``: Sebuah pointer ke konstanta karakter (``const char``) yang berisi data yang akan dikirim melalui soket.

2. Pada baris pertama fungsi, terdapat pemanggilan fungsi ``send`` yang digunakan untuk mengirim data melalui soket. Argumen-argumen yang diberikan ke fungsi ``send`` adalah:

    - ``socketDescriptor``: Deskriptor soket yang ditentukan saat melakukan koneksi.
    - ``data``: Data yang akan dikirim melalui soket.
    - ``strlen(data)``: Panjang data yang akan dikirim, yang diperoleh dengan menggunakan fungsi ``strlen`` untuk menghitung jumlah karakter dalam string ``data``.
    - ``0``: Opsi tambahan yang dapat digunakan dalam pengiriman soket. Nilai 0 digunakan untuk pengiriman yang normal tanpa opsi tambahan.
    
Dengan pemanggilan fungsi ``send`` tersebut, data yang diidentifikasi oleh ``data`` akan dikirim melalui soket yang ditentukan oleh ``socketDescriptor``. Fungsi ini tidak mengembalikan nilai apapun.

Fungsi ``send`` mengirim data dalam bentuk byte melalui soket yang terhubung. Perlu diingat bahwa fungsi ini hanya mengirim data dan tidak menjamin bahwa data tersebut akan diterima oleh penerima dengan sempurna. Penerima juga perlu melakukan operasi membaca (receive) untuk mengambil data yang dikirimkan.

```c
bool receiveData(int socketDescriptor, char *buffer, int bufferSize) {
    int readValue = recv(socketDescriptor, buffer, bufferSize, 0);
    if (readValue == 0) {
        close(socketDescriptor);
        return false;
    }
    return true;
}
```
Fungsi ``receiveData`` digunakan untuk menerima data dari soket (socket). Berikut adalah penjelasan lebih rinci tentang fungsi tersebut:

1. Fungsi ``receiveData`` memiliki tiga parameter:

    - ``socketDescriptor``: Sebuah nilai integer yang merupakan deskriptor soket (socket) yang akan digunakan untuk menerima data.
    - ``buffer``: Sebuah pointer ke char yang menyimpan data yang diterima dari soket.
    - ``bufferSize``: Sebuah nilai integer yang menentukan ukuran maksimum buffer untuk menerima data.

2. Pada baris pertama fungsi, terdapat pemanggilan fungsi ``recv`` yang digunakan untuk menerima data melalui soket. Argumen-argumen yang diberikan ke fungsi ``recv`` adalah:

    - ``socketDescriptor``: Deskriptor soket yang ditentukan saat melakukan koneksi.
    - ``buffer``: Buffer yang digunakan untuk menyimpan data yang diterima.
    - ``bufferSize``: Ukuran maksimum buffer untuk menerima data.
    - ``0``: Opsi tambahan yang dapat digunakan dalam penerimaan soket. Nilai 0 digunakan untuk penerimaan yang normal tanpa opsi tambahan.

3. Fungsi ``recv`` akan mengembalikan jumlah byte yang berhasil dibaca dari soket. Nilai ini disimpan dalam variabel ``readValue``.

4. Pada baris keempat, dilakukan pengecekan jika ``readValue`` adalah 0. Jika demikian, berarti tidak ada data yang diterima dari soket dan koneksi soket ditutup menggunakan fungsi ``close``. Fungsi ``receiveData`` mengembalikan nilai ``false`` untuk menandakan bahwa penerimaan data gagal.

5. Jika kondisi pada langkah sebelumnya tidak terpenuhi, artinya data telah berhasil diterima dari soket. Fungsi ``receiveData`` mengembalikan nilai ``true`` untuk menandakan bahwa penerimaan data berhasil.

Dengan demikian, fungsi ``receiveData`` digunakan untuk menerima data dari soket yang ditentukan dan menyimpannya dalam buffer yang disediakan. Fungsi ini mengembalikan ``true`` jika penerimaan data berhasil dan ``false`` jika tidak ada data yang diterima atau terjadi kesalahan.

Harap diingat bahwa penerimaan data melalui soket bersifat blokir, yang berarti fungsi ``recv`` akan menunggu hingga ada data yang tersedia untuk dibaca atau koneksi soket ditutup.

```c
void handleDatabaseChange(int socketDescriptor, const char *databaseName) {
    char receive[1000];
    do {
        memset(receive, 0, sizeof(receive));
        receiveData(socketDescriptor, receive, sizeof(receive));
        if (strcmp(receive, "Done") != 0) {
            printf("%s\n", receive);
            char command[1000];
            strcpy(command, "continue");
            sendData(socketDescriptor, command);
        }
    } while (strcmp(receive, "Done") != 0);
}
```
Kode tersebut merupakan implementasi dari fungsi ``handleDatabaseChange``. Fungsi ini bertujuan untuk menangani perubahan database dengan membaca pesan-pesan yang diterima dari soket dan melakukan tindakan berdasarkan pesan tersebut. Berikut adalah penjelasan lebih detail mengenai kode tersebut:

1. Fungsi ``handleDatabaseChange`` memiliki dua parameter:

    - ``socketDescriptor``: Sebuah nilai integer yang merupakan deskriptor soket (socket) yang akan digunakan untuk menerima pesan-pesan dari server.
    - ``databaseName``: Sebuah pointer ke konstanta karakter (const char) yang berisi nama database yang akan diubah.

2. Pada baris pertama fungsi, dideklarasikan array ``receive`` dengan ukuran 1000 yang akan digunakan untuk menyimpan pesan yang diterima dari soket.

3. Selanjutnya, dilakukan perulangan ``do-while`` yang akan berjalan hingga pesan yang diterima adalah ``"Done"``. Ini menunjukkan bahwa perubahan database telah selesai.

4. Di dalam perulangan, pertama-tama array ``receive`` diisi dengan nol menggunakan fungsi ``memset`` untuk mengosongkannya dan mempersiapkan buffer untuk menerima pesan yang baru.

5. Kemudian, pemanggilan fungsi ``receiveData`` dilakukan untuk menerima pesan dari soket dan menyimpannya dalam array ``receive`` dengan menggunakan ukuran buffer ``sizeof(receive)``.

6. Selanjutnya, terdapat kondisi ``if`` yang memeriksa apakah pesan yang diterima bukanlah "Done". Jika kondisi tersebut terpenuhi, maka pesan akan dicetak ke layar menggunakan ``printf``, kemudian dibuat perintah "continue" dalam bentuk string dan disimpan dalam array ``command``. Setelah itu, pemanggilan fungsi ``sendData`` dilakukan untuk mengirim perintah ``"continue"`` ke soket.

7. Perulangan akan terus berjalan selama pesan yang diterima bukanlah "Done". Begitu pesan menjadi "Done", perulangan akan berhenti dan fungsi ``handleDatabaseChange ``selesai.

Dengan demikian, fungsi ``handleDatabaseChange`` bertanggung jawab untuk menerima pesan-pesan dari soket yang berhubungan dengan perubahan database. Pesan-pesan tersebut akan dicetak ke layar dan perintah ``"continue"`` akan dikirim kembali ke soket sampai pesan ``"Done"`` diterima, menandakan bahwa perubahan database telah selesai.

Harap dicatat bahwa kode tersebut mengasumsikan bahwa fungsi ``receiveData, sendData, strcmp, printf, memset, strcpy``, dan variabel lain yang digunakan dalam kode telah didefinisikan dan berfungsi dengan benar di bagian lain dari program.

```c
int main(int argc, char const *argv[]) {
    if (!validateArguments(argc, argv)) {
        printf("Error: Invalid arguments.\n");
        return -1;
    }

    struct sockaddr_in serverAddress;
    int socketDescriptor = createSocketAndConnect(&serverAddress);
    if (socketDescriptor < 0) {
        printf("Error: Connection failed.\n");
        return -1;
    }

    char type[1024];
    if (getuid()) {
        sprintf(type, "%s %s dump", argv[2], argv[4]);
    } else {
        strcpy(type, "root dump");
    }
    sendData(socketDescriptor, type);

    char statusLogin[1000];
    if (!receiveData(socketDescriptor, statusLogin, sizeof(statusLogin))) {
        close(socketDescriptor);
        printf("Error: Authentication failed.\n");
        return -1;
    }

    char command[1000];
    sprintf(command, "USE %s", getuid() ? argv[5] : argv[1]);
    sendData(socketDescriptor, command);

    char receive[1000];
    strcpy(command, "continue");
    sendData(socketDescriptor, command);

    if (strncmp(receive, "Database changed to", 19) == 0) {
        handleDatabaseChange(socketDescriptor, argv[5]);
        close(socketDescriptor);
        return 0;
    }

    close(socketDescriptor);
    return 0;
}
```

Fungsi ``main`` adalah titik masuk utama dalam program. Di dalam fungsi ``main``, logika program dieksekusi. Berikut adalah penjelasan lebih rinci tentang kode dalam fungsi ``main``:

1. Pada baris pertama, terdapat pemanggilan fungsi ``validateArguments`` untuk memvalidasi argumen yang diberikan saat menjalankan program. Jika argumen tidak valid, pesan kesalahan dicetak ke layar, dan program keluar dengan kode status -1.

2. Selanjutnya, sebuah struktur ``serverAddress`` dari tipe ``sockaddr_in`` dideklarasikan untuk menyimpan informasi alamat server.

3. Dilakukan pemanggilan fungsi ``createSocketAndConnect`` untuk membuat soket (socket) dan melakukan koneksi ke server. Fungsi ini mengembalikan deskriptor soket (socket descriptor). Jika koneksi gagal, pesan kesalahan dicetak ke layar, dan program keluar dengan kode status -1.

4. Sebuah array ``typ`` dengan ukuran 1024 dideklarasikan untuk menyimpan tipe data yang akan dikirim ke server. Nilai yang akan disimpan dalam array ini bergantung pada kondisi ``getuid()``. Jika ``getuid()`` mengembalikan nilai selain 0 (artinya bukan root user), maka tipe data akan diatur sesuai dengan argumen yang diberikan. Jika ``getuid()`` mengembalikan nilai 0 (artinya root user), maka tipe data akan diatur sebagai "root dump". Setelah itu, pemanggilan fungsi ``sendData`` dilakukan untuk mengirimkan tipe data ke server.

5. Sebuah array ``statusLogin`` dengan ukuran 1000 dideklarasikan untuk menyimpan status hasil autentikasi dari server. Dilakukan pemanggilan fungsi ``receiveData`` untuk menerima data dari server dan menyimpannya dalam array ``statusLogin``. Jika penerimaan data gagal, soket ditutup, pesan kesalahan autentikasi dicetak ke layar, dan program keluar dengan kode status -1.

6. Sebuah array ``command`` dengan ukuran 1000 dideklarasikan untuk menyimpan perintah yang akan dikirim ke server. Pada baris selanjutnya, sebuah perintah ``"USE <database>"`` dibentuk menggunakan ``sprintf`` berdasarkan kondisi ``getuid()``. Jika ``getuid()`` mengembalikan nilai selain 0, maka argumen indeks ke-5 digunakan sebagai nama database. Jika getuid() mengembalikan nilai 0, maka argumen indeks ke-1 digunakan sebagai nama database. Setelah itu, pemanggilan fungsi ``sendData`` dilakukan untuk mengirim perintah ke server.

7. Sebuah array ``receive`` dengan ukuran 1000 dideklarasikan untuk menyimpan pesan yang diterima dari server. Kemudian, perintah ``"continue"`` disimpan dalam array ``command``. Pemanggilan fungsi ``sendData`` dilakukan untuk mengirim perintah ``"continue"`` ke server.

8. Dilakukan pengecekan jika pesan yang diterima dari server mengawali dengan teks "Database changed to" sepanjang 19 karakter menggunakan fungsi ``strncmp``. Jika kondisi tersebut terpenuhi, berarti perubahan database telah berhasil dilakukan, dan fungsi ``handleDatabaseChange`` dipanggil untuk menangani perubahan database dengan mengirimkan deskriptor soket dan nama database yang diterima sebagai argumen. Setelah itu, soket ditutup, dan program keluar dengan kode status 0.

9. Jika kondisi pada langkah sebelumnya tidak terpenuhi, maka soket ditutup, dan program keluar dengan kode status 0.

Ini adalah struktur umum dari logika yang dilakukan dalam fungsi ``main``. Fungsi ini bertanggung jawab untuk mengatur aliran program dan berinteraksi dengan soket untuk mengirim dan menerima data dari server.

# Permasalahan
Belum selesai sepenuhnya karena:
- Waktu pengerjaan
- Komplikasi soal